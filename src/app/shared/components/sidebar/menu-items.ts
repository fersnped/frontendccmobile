
export class DocMenus {
  modulo: string;
  path?: string;
  icon: string;
  nombre?: string;
  visible?: boolean;
}

export class DocAccess {
  nombre: string;
  accesos: DocMenus[]
  icon: string;
}


export const MENUS: DocAccess[] = [
  {
    nombre: "Catálogos",
    icon: 'reorder',
    accesos: [
      {
        nombre: 'Banners',
        modulo: 'BANNERS',
        path: '/banners',
        icon: 'view_carousel',
        visible: true
      }
    ]
  }, {
    nombre: "Operaciones",
    icon: 'check_box',
    accesos: [
      {
        nombre: 'Ventas',
        modulo: 'VENTAS',
        path: '/sale',
        icon: 'attach_money',
        visible: true
      },
      {
        nombre: 'Activaciones y Recargas',
        modulo: 'ACTIVACIONES_Y_RECARGAS',
        path: '/security/activation',
        icon: 'check_circle',
        visible: true
      },
      {
        nombre: 'QR',
        modulo: 'QR',
        path: '/qr',
        icon: 'gradient',
        visible: true
      }
    ]
  }, {
    nombre: 'Reportes',
    icon: 'gradient',
    accesos: [
      {
        nombre: 'Reporte de recargas',
        modulo: 'REPORTE_DE_RECARGAS',
        path: '/security/recargas/reportes',
        icon: 'gradient',
        visible: true
      },
      {
        nombre: 'Consultas',
        modulo: 'CONSULTAS',
        path: '/reports/queries',
        icon: 'find_in_page',
        visible: true
      },
      {
        nombre: 'Inventario por vendedor',
        modulo: 'INVENTARIO_POR_VENDEDOR',
       path: '/reports/inventory-seller',
        icon: 'recent_actors',
        visible: true
      }, {
        nombre: 'Reporte de Ventas',
        modulo: 'REPORTE_DE_VENTA',
        path: '/reports/reportSell',
        icon: 'insert_chart',
        visible: true
      }, {
        nombre: 'Reporte de estatus por serie',
        modulo: 'REPORTE_ESTATUS_POR_SERIE',
        path: '/reports/reportStatus',
        icon: 'insert_chart',
        visible: true
      },{
        nombre: 'Reporte de activaciones',
        modulo:'REPORTE_ACTIVACIONES',
        path:'/reports/activations',
        icon:'insert_chart',
        visible:true
      },{
        nombre: 'Reporte de usuarios activos',
        modulo:'USUARIO_ACTIVO',
        path:'/reports/activo',
        icon:'insert_chart',
        visible:true
      }
    ]
  }, {
    nombre: 'Seguridad',
    icon: 'security',
    accesos: [
      {
        nombre: 'Usuarios',
        modulo: 'USUARIOS',
        path: '/security/users',
        icon: 'people',
        visible: true
      },
      {
        nombre: 'Roles y permisos',
        modulo: 'ROLES_Y_PERMISOS',
        path: '/security/rolls',
        icon: 'people',
        visible: true
      },
      {
        nombre: 'Cambiar contraseña',
        modulo: 'CHANGE_PASSWORD',
        path: '/security/change-password',
        icon: 'lock',
        visible: true
      }

    ]
  }







];

