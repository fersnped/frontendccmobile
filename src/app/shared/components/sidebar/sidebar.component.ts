import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DocMenus, MENUS, DocAccess } from './menu-items';
import { SidebarService } from 'src/app/services/sidebar/sidebar.service';


declare let $: any;

@Component({
  selector: 'ag-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @ViewChild('sidenav') private sideNav: ElementRef;
  @ViewChild('menuCollap') private menuCollap: ElementRef;
  modulos: DocAccess[] = [];
  nMenuOpen: any = {
    nModulo: 0
  }
  constructor(
    private _router: Router,
    private _auth: AuthService,
    private _sidebar: SidebarService
  ) {
  }

  ngOnInit() {
    //$('.side-nav a.close-sidenav').sideNav('hide');
    $('.mCustomScrollbar .content').mCustomScrollbar({
      axis: 'y',
      theme: 'minimal-dark'
    });
    $('#main-menu .close-sidenav.waves-effect').click(function () {
      window.scrollTo(0, 0);
    });
  }

  ngAfterViewInit() {
    this.getModules();
    $(this.sideNav.nativeElement).sidenav();
    $(this.menuCollap.nativeElement).collapsible();
    setTimeout(() => {
      this.nMenuOpen = this._sidebar.getMenuOption()
      if (this.nMenuOpen.nModulo != null && this.nMenuOpen.isClicked) {
        this.openMenu();
        this._sidebar.setIsClicked(false)
      }
    }, 200);


  }

  logOut(): void {
    this._auth.destroySession();
		this._router.navigate(['/login']);
  }

  getModules() {
    const user = this._auth.getUser();
    user.modulos.forEach((module) => {
      let docAccess = MENUS.find((access) => module.nombre == access.nombre)
      let accesModuls: DocAccess = {
        nombre: docAccess.nombre,
        accesos: [],
        icon: docAccess.icon
      }
      module.accesos.forEach(element => {
        let docMenus = MENUS.find((mod) => mod.nombre == accesModuls.nombre).accesos.find((acc) => acc.modulo == element.MODULO && acc.visible == true);
        if (docMenus !== undefined && docMenus !== null) {
          docMenus.nombre = element.nombre !== undefined ? element.nombre : '';
          accesModuls.accesos.push(docMenus);
        }
      });
      this.modulos.push(accesModuls)
    });
  }

  findMenu(m) {
    $('.collapsible').collapsible();

    const user = this._auth.getUser();
    return user.modulos.forEach(element => {
      element.accesos.find(result => {
        return result.MODULO === m;
      });
    });
  }


  goToMenu(menu) {
    this._sidebar.setMenuOption(menu)
  }

  isClicked() {
    this._sidebar.setIsClicked(true)
  }

  openMenu() {
    $(this.menuCollap.nativeElement).collapsible('open', this.nMenuOpen.nModulo)
  }

}
