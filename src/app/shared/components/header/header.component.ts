import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
	selector: 'ag-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	constructor(
		private _router: Router,
		private _auth: AuthService
	) { }

	ngOnInit() {
	}

	logOut(): void {
		this._auth.destroySession();
		this._router.navigate(['/login']);
	}

}
