import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleActivationComponent } from './toggle-activation.component';

describe('ToggleActivationComponent', () => {
  let component: ToggleActivationComponent;
  let fixture: ComponentFixture<ToggleActivationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleActivationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleActivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
