import { Component, OnInit } from '@angular/core';

declare let $: any;
@Component({
	selector: 'ag-carousel',
	templateUrl: './carousel.component.html',
	styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

	constructor() { }

	ngOnInit() {
	}

	ngAfterViewInit() {
		$('.carousel.carousel-slider').carousel({
			fullWidth: true,
			indicators: true,
			noWrap: true
		});
	}

}
