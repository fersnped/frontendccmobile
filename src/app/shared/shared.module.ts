import { NgModule } from '@angular/core';

// Components
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { CarouselComponent } from './components/carousel/carousel.component';

// Module
import { RouterModule } from '@angular/router';
import { ToggleActivationComponent } from './components/toggle-activation/toggle-activation.component';
import { LoaderComponent } from './components/loader/loader.component';
import { Util } from '../common/util';
import { QRCodeModule } from 'angularx-qrcode';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule, MatInputModule, MatRippleModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { DeviceDetectorModule } from 'ngx-device-detector';
@NgModule({
  declarations: [
    SidebarComponent,
    HeaderComponent,
    CarouselComponent,
    ToggleActivationComponent,
    LoaderComponent
  ],
  exports: [
    CommonModule,
    SidebarComponent,
    HeaderComponent,
    CarouselComponent,
    ToggleActivationComponent,
    QRCodeModule,
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    CdkTableModule,
    MatPaginatorModule,
    MatTableModule,
    DeviceDetectorModule
  ],
  imports: [
    RouterModule,
    QRCodeModule,
    CommonModule,
    BrowserModule,
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    CdkTableModule,
    MatPaginatorModule,
    MatTableModule,
    DeviceDetectorModule.forRoot()
  ], providers: [
    Util
  ]
})
export class SharedModule {
}
