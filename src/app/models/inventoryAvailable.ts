export interface IInventoryAvailable {
    fechaActivacion?: string;
    fechaFacturacion?: string;
    fechaOrdenVenta?: string;
    iccid?: string;
    imei?: string;
    noFactura?: string;
    noRemisionAG?: string;
    numeroCelular?: string;
    oVenta?: Number;
    vendedor?: string;
    sucursal?:string;
    cliente?:string;
    folioVenta?:string;
}
export class InventoryAvailable implements IInventoryAvailable {
    constructor() { }
}