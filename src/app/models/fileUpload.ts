export interface FileUpload {
    name?: string;
    size?: number;
    base64?: any;
    file?: File;
}

export class FileUploadC implements FileUpload {
    constructor() { }
}