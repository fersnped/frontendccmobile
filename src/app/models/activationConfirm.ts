import {IProducto, Producto} from './producto';
import {IPlan} from './plan';
import {ITarifa} from './tarifa';
import {Ciudad, ICiudad} from './ciudad';

export interface IActivationConfirm {

  usuarioId?: number;
  producto?: IProducto;
  plan?: IPlan;
  tarifa?: ITarifa;
  ciudad?: ICiudad;
  imei?: string;
  iccid?: string;
}

export class ActivationConfirm implements IActivationConfirm {
  ciudad: ICiudad;
  iccid: string;
  imei: string;
  plan: IPlan;
  producto: IProducto;
  tarifa: ITarifa;

  constructor() {

    this.ciudad = new Ciudad();
    this.plan = new class implements IPlan {
      id: number;
      nombre: string;
    };
    this.producto = new Producto();
    this.tarifa = new class implements ITarifa {
      id: number;
      nombre: string;
    };
  }
}

