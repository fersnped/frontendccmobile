import {IPlan} from './plan';

export interface IProducto {
  id?: number;
  producto?: string;
  planes?: IPlan[];
}

export class Producto implements IProducto {
  constructor() {
  }
}
