export interface IBanners {
    id?: number;
    nombre?: string;
    descripcion?: string;
    Fecha_Inicio_Publicacion?: string;
    image?: string;
}

export class Banners implements IBanners{
    constructor(){}
}