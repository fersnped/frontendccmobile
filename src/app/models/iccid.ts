export interface IIccid {
  Codigo?: number;
  TipoProductoId?: number;
  DescripcionProducto?: string;
  SucursalId?: string;
  StatusInventarioId?: number;
  UsuarioId?: string;
  NoRemision?: string;
  NoFactura?: string;
  FechaFacturacion?: string;
}

export class Iccid implements IIccid {
  constructor() {
  }
}
