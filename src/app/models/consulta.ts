export interface IConsulta {
    numeroTelefono?: string;
    imei?: string;
    iccid?: string;
    estatusDescripcion?: string;
    estatusTelcel?: string;
    error?: string;
    producto?: string;
    fechaActivacion?: string;
    tecnologia?: string;
    fechaOrdenVenta?: string;
    tarifa?: String;
    sucursal?: string;
    usuarioRecarga?: string;
    clienteActivacion?: string
}

export class Consulta implements IConsulta {
    constructor() { }
}