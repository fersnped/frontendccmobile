export interface IRegisteredLines {
    imei?: string;
    iccid?: string;
    descripcion?: string;
}

export class RegisteredLines implements IRegisteredLines {
    constructor() { }
}