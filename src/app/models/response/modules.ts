import { IModule } from "../module";

export interface IModulesResponse {

    modules?: IModule[];

}

export class ModulesResponse implements IModulesResponse {
    constructor() { }
}