import { IBanners } from "../banners";

export interface IBannersRequest {
    banners?: IBanners[]
}

export interface IBannerRequest {
    banner?: IBanners;
}

export class BannersRequest implements IBannersRequest {

}

export class BannerRequest implements IBannerRequest {

}