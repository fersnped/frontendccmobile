import { IConsulta } from "../consulta";

export interface IConsultaResponse {
    consultas?: IConsulta[]
}

export class ConsultaResponse implements IConsultaResponse {
    constructor() { }
}