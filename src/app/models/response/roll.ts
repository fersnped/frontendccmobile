import { IRoll } from 'src/app/models/roll';

export interface IRollsResponse {
	rolls: IRoll;
}

export interface IRollResponse {

}