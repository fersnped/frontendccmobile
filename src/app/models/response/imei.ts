export interface IImei {
    codigo?: number;
    descripcion_producto?: string;
    sucursal_id?: string;
    status_inventario_id?: number;
    no_remision?: string;
    no_factura?: string;
    fecha_facturacion?: string;
  }
  
  export class Imei implements IImei {
    constructor() {
    }
  }
  