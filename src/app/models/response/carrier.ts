import { ICarrier } from "../carrier";

export interface ICarriersResponse {
    carrier?: ICarrier[];
}

export class CarriersResponse implements ICarriersResponse {

    constructor() { }
}