import { IReportsActivations } from "../reportsActivations";

export interface IReportsActivationsResponse {
    activations?: IReportsActivations[]
}

export class ReportsActivationsResponse implements IReportsActivationsResponse {
    constructor() { }
}