import { IClient } from "../client";

export interface ISubdistribuidorResponse {
    subdistribuidor?: IClient[]
}

export class SubdistribuidorResponse implements ISubdistribuidorResponse {
    constructor() { }
}

