import { IUsuariosVendedores } from "../usuariosVendedores";

export interface IUsuariosVendedoresResponse {
    vendedores?: IUsuariosVendedores[];
}

export class UsuariosVendedoresResponse implements IUsuariosVendedoresResponse {
    constructor() { }
}

