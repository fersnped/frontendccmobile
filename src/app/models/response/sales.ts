import { ISales } from "../sales";

export interface ISalesResponse {
    ventas?: ISales[];
}

export class SalesResponse implements ISalesResponse {
    constructor() { }
}

export interface ISaleResponse {
    venta?: ISales;
}

export class SaleResponse implements ISaleResponse {
    constructor() { }
}