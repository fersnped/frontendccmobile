import { IInventorySeller } from "../inventorySeller";

export interface IInventorySellerResponse {
    inventario?: IInventorySeller[];

}

export class InventorySellerResponse implements IInventorySellerResponse {
    constructor() { }
}