import { IInventoryAvailable } from "../inventoryAvailable";

export interface IInventoryAvailableResponse {
    inventory?: IInventoryAvailable[]
}

export class InventroyAvailableResponse implements IInventoryAvailableResponse {
    constructor() { }
}