import { IUser } from 'src/app/models/user';

export interface ILoginResponse {
	user: IUser;
	token: string;
}

export interface IChangePasswordResponse {
	message?: string;
}

export interface IResetPasswordResponse {
	message?: string;
}