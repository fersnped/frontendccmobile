export interface IApiRealResponse<T> {
	responseResult?: T;
	responseMessage?: string;
	success?: boolean;
	responseErrors?: any;
}

export interface IApiResponse<T> {
	data?: T;
	message?: string;
	httpCode?: number;
	success?: boolean;
	errors?: any;
}