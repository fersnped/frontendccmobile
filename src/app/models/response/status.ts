export interface IStatus {
    inventarioAg: IInventarioAg;
    activacion: IActivacion;
    venta: IVenta;
    recarga: IRecarga;
}

export interface IActivacion {
    iccid: string;
    imei: string;
    folio: string;
    numero: number;
    tipoActivacion: string;
    usuarioActivacion: string;
    observacionesActivacion: string;

}
export interface IInventarioAg {
    fechaFacturacion: Date;
    numeroFactura: string;
    numeroRemision: string;
    statusInventario: string;
    sucursal: string;
    tipoProducto: string;
    usuarioAsociado: string;
}

export interface IVenta {
    cliente: string;
    estado: string;
    fechaVenta: Date;
    folio: string;
    vendedor: string;
}

export interface IRecarga {
    estatusRecarga: string;
    fechaRecarga: Date;
    folio: string;
    importe: number;
    usuarioRecarga: string;

}