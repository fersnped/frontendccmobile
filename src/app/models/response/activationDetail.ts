import {IActivationDetail} from '../activationDetail';

export interface IActivacionesDetailResponse {
  activacion: IActivationDetail;
}

export interface IActivacionesDetailResponse {
}
