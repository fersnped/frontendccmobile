import { IFilesQR } from "../filesQr";

export interface IFilesQRResponse {
    files?: IFilesQR[]
}

export class FilesQRResponse implements IFilesQRResponse {
    constructor() { }
}

