export interface IActivateKit {
  producto?: number;
  plan?: number;
  tarifa?: number;
  icci?: string;
  imei?: string;
  cuidad?: number;
}

export class ActivateKit implements IActivateKit {
  cuidad?: number;
  icci?: string;
  imei?: string;
  plan?: number;
  producto?: number;
  tarifa?: number;

  constructor() {
  }
}
