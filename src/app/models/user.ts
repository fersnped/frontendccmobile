import { IModule, IAccess } from 'src/app/models/module';
import { ICarrier, Carrier } from './carrier';
import { IRoll, Roll } from './roll';

export interface IUser {
    id?: number;
    correo?: string;
    nombre?: string;
    apellido_Materno?: string;
    apellido_Paterno?: string;
    nombre_Usuario?: string;
    primer_Login?: boolean;
    telefono?: string;
    rfc?: string;
    razon_social?: string;
    tipo_persona?: number;
    modulos?: IAccess[];
    activo?: boolean | number;
    carrier: ICarrier;
    rol: IRoll;
    permisos_especiales?: boolean | number;
    direccion?: string;
    typeUser?: string;
    numero_Interno_AG_ID?: number;
    perfilId?: number;
}

export class User implements IUser {
    modulos: IModule[];
    carrier: ICarrier;
    rol: IRoll;
    typeUser: string;
    constructor() {
        this.typeUser = '0';
        this.modulos = [];
        this.carrier = new Carrier();
        this.rol = new Roll();
    }
}