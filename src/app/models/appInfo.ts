export class AppInfo {
    plataforma?: number;
    marcaOnavegador?: string;
    modeloOversionNavegador?: string;
    os?: number
    versionOs?: string;
    versionApp?: string;
    constructor() {

    }
}


export enum OSVersions {
    iOS = 1,
    Android = 2,
    Linux = 3,
    Windows = 4,
    MacOs = 5
}

