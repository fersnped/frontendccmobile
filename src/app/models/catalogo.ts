import {ITarifa} from './tarifa';
import {IProducto} from './producto';

export interface ICatalogo {
  productos?: IProducto[];
  tarifas?: ITarifa[];
}

export class Catalogo implements ICatalogo {
  constructor() {
  }
}
