export interface IModule {
  acceso_id?: number;
  id?: number;
  nombre?: string;
  descripcion?: string;
  accesos?: any[];
  permiso?: number;
  tipo_permiso?: number;
  MODULO?: string;
}

export class Module implements IModule {
  constructor() { }
}


export interface IAccess {
  nombre?: string;
  accesos?: IModule[]
}

export class Access implements IAccess {
  constructor() { }
}