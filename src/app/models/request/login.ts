export interface ILoginRequest {
	user: string;
	password: string;
}

export interface IChangePasswordRequest {
	user: number;
	passwords: {
		old: string;
		new: string;
	}
}

export interface IResetPasswordRequest {
	mail: string;
}