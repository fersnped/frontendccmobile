import { IRoll } from 'src/app/models/roll';

export interface IRollRequest {
	roll: IRoll;
}