import { IDetailSale } from "../detailSale";

export class ProductRequest {
  ejecutivoId?: number;
  subdistribuidorId?: number;
  productos?: IDetailSale[];

  constructor() {
    this.subdistribuidorId = -1;
    this.productos = [];
  }
}
