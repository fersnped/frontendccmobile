import { IUser } from 'src/app/models/user';
import { IModule, Module } from '../module';

export interface IUserRequest {
	id?: number;
	nombre?: string;
	nombre_Usuario?: string;
	apellido_Paterno?: string;
	apellido_Materno?: string;
	telefono?: string;
	email?: string;
	rolId?: number;
	carrierId?: number;
	rfc?: string;
	razon_social?: string;
	tipo_persona?: number;
	modulos?: Module[];
	permisos_especiales?: boolean;
	typeUser?: string;
	numero_Interno_AG_ID?: number;
	direccion?: string;
	perfilId?: number;
}

export class UserRequest implements IUserRequest {
	modulos?: Module[] = [];
	
	constructor() {

	}
}