import { AppInfo } from "../appInfo";

export interface IActivationRequest {
  usuarioId?: string;
  productoId?: string;
  planId?: string;
  tarifaId?: string;
  iccid?: string;
  imei?: string;
  ciudadId?: string;
  appInfo?: AppInfo;
}

export class ActivationRequest implements IActivationRequest {

  constructor() {
  }
}
