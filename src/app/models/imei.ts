export interface IImei {
  Codigo?: number;
  TipoProductoId?: number;
  DescripcionProducto?: string;
  SucursalId?: string;
  StatusInventarioId?: number;
  UsuarioId?: string;
  NoRemision?: string;
  NoFactura?: String;
  FechaFacturacion?: String;
}

export class Imei implements IImei {
  constructor() {
  }
}
