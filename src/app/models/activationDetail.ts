export interface IActivationDetail {
  Folio?: number;
  Producto?: string;
  Plan?: string;
  Tarifa?: string;
  ICCID?: string;
  IMEI?: string;
  Ciudad?: string;
  Fecha_creacion?: string;
  Hora_creacion?: string;
  EstatusId?: number;
  Estatus?: string;
  numero?: string;
  recarga?: number;
  status_recarga?: number;
  Error_recarga?: string;
  message?: string;
}

export class ActivationDetail implements IActivationDetail {
  constructor() {
  }
}
