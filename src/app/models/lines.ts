export interface ILines {
    id?: string;
    numero?: string;
    imei?: string;
    iccid?: string;
    importe?: number;
    descripcion?: string;
    fecha_vigencia?: string;
    src?: string;
}
export class Lines implements ILines {
    constructor() { }
}