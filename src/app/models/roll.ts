import { IModule } from "./module";

export interface IRoll {
	id: number;
	nombre?: string;
	descripcion?: string;
	activo?: boolean | number;
	modulos?: IModule[];
}

export class Roll implements IRoll {
	id: number;
	constructor() {
		this.id = 0;
	}
}