export interface IRecargasRealizadas extends IRecargasDisponibles {
  fecha_actualizacion?: string;
  nombre_usuario?: string;

}

export class RecargasRealizadas implements IRecargasRealizadas {
  constructor() {
  }
}


export interface IRecargasDisponibles {

  id?: number;
  numero?: string;
  imei?: string;
  iccid?: string;
  recarga?: number;
  producto?: string;
  fecha_activacion?: string;
  fecha_vigencia?: string;

}

export class RecargasDisponibles implements IRecargasDisponibles {
  constructor() {
  }
}

