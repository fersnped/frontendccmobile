export interface IReportsActivations {
    fechaActivacion?: string;
    horaActivacion?: string;
    imei?: string;
    iccid?: string;
    numeroCelular?: string;
    plan?: string;
    tarifa?: string;
    activador?: string;
    plataforma?: string;
}

export class ReportsActivations implements IReportsActivations {
    constructor() { }
}