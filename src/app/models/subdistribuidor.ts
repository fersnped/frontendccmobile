export interface ISubdistribuidor {
  usuario_id?: number;
  nombre?: string;
  rfc?: string;
  tipo_usuario_id?: string;
  direccion?: string;
}

export class Subdistribuidor implements ISubdistribuidor {
  constructor() {
  }
}
