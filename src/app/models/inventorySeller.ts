export interface IInventorySeller {
    codigo?: string;
    descripcionProducto?: string;
    noRemision?: string;
    noFactura?: string;
    fechaFacturacion?: string;
    vendedor?: string;
    sucursal?: string;
}

export class InventorySeller implements IInventorySeller {
    constructor() {
    }
}