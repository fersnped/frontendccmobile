export interface IActivation {
  Folio?: number;
  IMEI?: string;
  ICCID?: string;
  TipoActivacion?: string;
  Numero?: string;
  Estatus?: string;
}

export class Activation implements IActivation {
  constructor() {
  }
}
