export class Label {
	id?: number
	name: string;
	active: boolean = true
	createdAt?: Date
	updatedAt?: Date
	data?: any
}
