import { IClient } from "./client";
import { IDetailSale } from "./detailSale";

export interface ISales {
    id?: number;
    Subdistribuidor_id?: number;
    fecha_creacion?: string;
    subdistribuidor?: IClient;
    detalle_venta?: IDetailSale[];
    // subdistribuidor:ISubDistribuido;
}

export class Sales implements ISales {
    constructor() { }
}
export interface ISubDistribuido {
    usuario_id: number;
    nombre: string;
    direccion: string;
}