export interface IClient {
    id?: number;
    usuario_id?: number
    nombre?: string;
    apellidoPaterno?: string;
    apellidoMaterno?: string;
    razon_social?: string;
    direccion?: string;
    tipo_usuario_id?: number;
    rfc?: string;
}

export class Client implements IClient {
    constructor() { }
}