import { AbstractControl } from '@angular/forms';
export class PasswordValidation {
	static MatchPassword(control: AbstractControl) {
		let password = control.get('new').value; // to get value in input tag
		let confirmPassword = control.get('confirm').value; // to get value in input tag

		if (password !== confirmPassword) {
			control.get('confirm').setErrors({ matchpassword: true });
		} else {
			return null;
		}
	}
}