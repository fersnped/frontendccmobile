import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filtration"
})
export class FiltrationPipe implements PipeTransform {
  transform(value: Array<any>, ...arg): Array<any> {
    //arg[0] Busqueda
    //arg[1] Columnas que pertenecen a la busqueda
    if (!value || value.length <= 0) return [];
    let [{ numero, iccid, nombre_usuario }] = arg;
    if (!numero && !iccid && !nombre_usuario) return value;

    return value.filter(dato => {
      if (numero && iccid) {
        return (
          this.getIndexOf(dato, numero, "numero") >= 0 &&
          this.getIndexOf(dato, iccid, "iccid") >= 0
        );
      }
      if (numero && nombre_usuario) {
        return (
          this.getIndexOf(dato, numero, "numero") >= 0 &&
          this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0
        );
      }
      if (iccid && nombre_usuario) {
        return (
          this.getIndexOf(dato, iccid, "iccid") >= 0 &&
          this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0
        );
      }
      if (iccid && nombre_usuario && numero) {
        return (
          this.getIndexOf(dato, numero, "numero") >= 0 &&
          this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0 &&
          this.getIndexOf(dato, iccid, "iccid") >= 0
        );
      }
      return (
        this.getIndexOf(dato, numero, "numero") >= 0 ||
        this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0 ||
        this.getIndexOf(dato, iccid, "iccid") >= 0
      );
    });
  }
  getIndexOf(dato, other, type: string) {
    if (other) {
      return String(dato[type])
        .toLowerCase()
        .trim()
        .indexOf(other);
    } else {
      return -1;
    }
  }
}
// data.reduce((before,after)=>{
//     if (numero && iccid && nombre_usuario) {
//         if (after.numero==numero && after.iccid==iccid) {

//         }
//     }

//     iccid
//     nombre_usuario
//     return
// })
