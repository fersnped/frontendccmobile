import { NgModule } from '@angular/core';
import { FilterPipe } from './filter.pipe';
import { CommonModule } from '@angular/common';
import { DomseguroPipe } from './domseguro.pipe';
import { OrderPipe } from './order.pipe';
import { FiltrationPipe } from "./filtration.pipe";


// Pipes
@NgModule({
  declarations: [
    FilterPipe,
    FiltrationPipe,
    DomseguroPipe,
    OrderPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FilterPipe,
    FiltrationPipe,
    DomseguroPipe
  ]
})
export class PipesModule {}
