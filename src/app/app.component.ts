import { Component, OnInit } from '@angular/core';
import { LoaderService } from './services/shared/loader.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	title = 'Teledata';

	constructor() { }

	ngOnInit() {
	}

}
