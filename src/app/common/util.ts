import { Injectable } from '@angular/core';

import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';


declare let $: any;
@Injectable()
export class Util {

    constructor(private http: Http) { }

    public triggerClick(selectorID: string): void {
        $(`#${selectorID}`).trigger('click');
    }

    public displayFile(event: any): Promise<any> {
        return new Promise((resolve, reject) => {
            if (event.target.files && event.target.files[0]) {
                var reader = new FileReader();
                let name = event.target.files[0].name;
                
                reader.onload = (loadEvent: any) => {
                    resolve({ base64: loadEvent.target.result, name: name, file: event.target.files[0], size: event.target.files[0].size });
                }
                
                reader.onerror = (error) => {
                    //console.log(error);
                    reject({ code: 'UPLOAD_ERROR', message: 'Error al subir el archivo' });
                };
                reader.readAsDataURL(event.target.files[0]);
                console.log("error");
            } else {
                reject({ code: 'NO_FILE', message: 'No se subio ningún archivo' });
            }
        });
    }
    public dragoverHandler(event: any) {
        event.preventDefault();
    }

    public dropHandler(event: any, types?: string[]): Promise<any[]> {
        event.preventDefault();
        let error = null;

        // If dropped items aren't files, reject them
        let dt = event.dataTransfer;
        return new Promise((resolve, reject) => {
            let filesToSend: any[] = [];
            if (dt.items) {
                // Use DataTransferItemList interface to access the file(s)

                for (let i = 0; i < dt.items.length; i++) {
                    if (dt.items[i].kind == 'file') {
                        let f = dt.items[i].getAsFile();

                        let arrFileName = f.name.split('.')
                        let extension = arrFileName[arrFileName.length - 1]
                        if (types.filter((item) => item == extension).length > 0) {
                            filesToSend.push({ data: dt.items[i].getAsFile(), name: f.name, size: dt.items[i].getAsFile().size });
                        } else {
                            error = 'Uno de los archivos no tiene el formato solicitado';
                        }
                    }
                }
            } else {
                // Use DataTransfer interface to access the file(s)
                for (let i = 0; i < dt.files.length; i++) {
                    //console.log("... file[" + i + "].name = " + dt.files[i].name);
                    filesToSend.push({ file: dt.files[i], name: dt.files[i].name });
                }
            }
            if (types && types.length > 0 && error) {
                return reject({ code: 'FILE_ERROR', message: error });
            }
            resolve(filesToSend);
        });
    }

    public dragendHandler(event: any) {
        event.dataTransfer.clearData();
    }

}