
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Components
import { AppComponent } from './app.component';
import { LobbyComponent } from './lobby/lobby.component';
import { LoginComponent } from './lobby/login/login.component';
import { RecoveryPasswordComponent } from './lobby/recovery-password/recovery-password.component';
import { FirstLoginComponent } from './lobby/first-login/first-login.component';

// Module
import { PagesModule } from './pages/pages.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {SaleComponent} from './pages/sale/sale.component'
import {NewSaleComponent} from './pages/sale/newSales/newSales.component'

// Routes
import { ROUTES_APP } from './app.routing';
import { ServicesModule } from './services/service.module';
import { PipesModule } from './pipes/pipes.module';
import { LoaderComponent } from './main/loader/loader.component';
import { MzModalModule } from 'ngx-materialize';
import { MzSelectModule, MzCheckboxModule, MzInputModule } from 'ngx-materialize';
import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule, MatInputModule, MatRippleModule, MatTableModule, MatPaginatorModule, MatIconModule } from '@angular/material';
import { NgxPaginationModule } from "ngx-pagination";


@NgModule({
  declarations: [
    LobbyComponent,
    LoginComponent,
    RecoveryPasswordComponent,
    AppComponent,
    FirstLoginComponent,
    LoaderComponent,
    SaleComponent,
    NewSaleComponent,

  ],
  imports: [
    ROUTES_APP,
    BrowserModule,
    BrowserAnimationsModule,
    PagesModule,
    ServicesModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    HttpClientModule,
    MzModalModule,
    MzSelectModule,
    MzCheckboxModule,
    MzInputModule,
    CdkTableModule,
    MatButtonModule, MatInputModule, MatRippleModule, MatTableModule, MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
