import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
// import * as html2canvas from "html2canvas";
import * as jspdf from "jspdf";
import { Label } from "src/app/models/label";
import { Lines } from "src/app/models/lines";
import { QrService } from "src/app/services/qr/qr.service";
import { AlertsService } from "src/app/services/shared/alerts.service";

declare let $: any;
@Component({
  selector: "ag-qr-detail",
  templateUrl: "./qr-detail.component.html",
  styleUrls: ["./qr-detail.component.scss"]
})
export class QrDetailComponent implements OnInit {
  @ViewChild("modalLabel") private modalLabel: ElementRef;
  @ViewChild("labels") labels: ElementRef;
  label: number;
  bFlagQrGenerated: boolean = false;
  bIsLoading: boolean = false;
  nIdDoc: number;
  sUrlDoc: string = "";
  lineasRegistrada: Lines[] = [];
  fileLoad: boolean = false;
  labelss: Label[] = [];
  loading:boolean=false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private qrService: QrService,
    private _alert: AlertsService,
    private cd: ChangeDetectorRef,
    private domSanitizer: DomSanitizer
  ) {}

  async ngOnInit() {
    try {
      let res = await this.qrService.getLabels();
      this.labelss = res.map(l => {
        return { ...l, data: JSON.parse(l.data) };
      }) as Label[];
    } catch (error) {
      this._alert.info({
        text: "Existio un error al obtener los diseños de los productos."
      });
    }
    this.activatedRoute.paramMap.subscribe(params => {
      this.nIdDoc = Number.parseInt(params.get("id"));
      this.getLines();
    });

    $(this.modalLabel.nativeElement).modal();
  }

  ngAfterViewInit() {}

  getLines() {
    this.qrService
      .getLineas(this.nIdDoc)
      .then((data: any) => {
        this.lineasRegistrada = data.slice(0, 500);
      })
      .catch((err: any) => {
        console.log(err);
        this._alert.error({ title: "", text: "" });
      });
  }

  generarQR() {
    this.bIsLoading = true;
    this.bFlagQrGenerated = true;
    setTimeout(() => {
      this.bIsLoading = false;
    }, 500);
  }
  onSelectLabel(label: number) {
    this.label = label;
  }

  imprimirEtiqueta() {
    if (!this.label)
      return this._alert.error({ text: "Seleccióna un producto" });
    this.loading=true;
    this.cd.detectChanges();
    // $(this.modalLabel.nativeElement).modal('open');
    var idPdf = this.nIdDoc;
    var result = this.qrService.downloadPdf(idPdf, this.label).then(result => {
      // console.log(result);
      // window.location = result.url;
      // this.sUrlDoc = result.url;
      setTimeout(() => {
        this.loading=false
        window.open(result.url, "_blank");
      }, 10000);
    });
  }

  downloadFile() {
    // var idPdf = this.nIdDoc;
    // this.qrService.downloadPdf(idPdf)
    //   .then((result) => {
    //     setTimeout(() => {
    //
    //     }, 2000)
    //     $(this.modalLabel.nativeElement).modal('close');
    //   })

    var splitText = this.sUrlDoc.split("/");
    window.open(this.sUrlDoc, "_blank");

    // var pom = document.createElement('a');
    // pom.setAttribute('href', 'data:application/octet-stream,' + encodeURIComponent(this.sUrlDoc));
    // setTimeout(() => {
    //   pom.setAttribute('download', splitText[splitText.length]);
    //   pom.style.display = 'none';
    //   document.body.appendChild(pom);
    //   pom.click();
    //   document.body.removeChild(pom);
    // }, 5000);
  }

  isMultiplo(num) {
    let n = num + 1;
    return Number.isInteger(n / 6);
  }

  // generatePDF() {
  //   this.bIsLoading = true;
  //   let data = document.getElementById("rowLabels");
  //   // let data = this.labels.nativeElement;
  //   html2canvas(data, {
  //     allowTaint: false,
  //     useCORS: true,
  //     logging: false
  //   }).then((canvas: any) => {
  //     var pdf = new jspdf("p", "pt", "letter");

  //     for (var i = 0; i <= Math.round(canvas.height / 980); i++) {
  //       var srcImg = canvas;
  //       var sX = 0;
  //       var sY = 980 * i; // start 980 pixels down for every new page
  //       var sWidth = canvas.width;
  //       var sHeight = 980;
  //       var dX = 0;
  //       var dY = 0;
  //       var dWidth = 820;
  //       var dHeight = 980;

  //       var pageCanvas = document.createElement("canvas");
  //       pageCanvas.setAttribute("width", "1165");
  //       pageCanvas.setAttribute("height", "980");
  //       // pageCanvas.width = 100 * 8.5;
  //       // pageCanvas.height = 100 * 10.25;
  //       var ctx = pageCanvas.getContext("2d");
  //       ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

  //       var canvasDataURL = pageCanvas.toDataURL("image/png", 1.0);
  //       var width = pageCanvas.width;
  //       var height = pageCanvas.clientHeight;
  //       pageCanvas.remove();
  //       if (i > 0) {
  //         pdf.addPage(); //8.5" x 11" in pts (in*72)
  //       }
  //       pdf.addImage(canvasDataURL, "PNG", 10, 30, width * 0.72, height * 0.62);
  //     }
  //     pdf.save(`PDFEtiquetas_${this.lineasRegistrada.length}.pdf`);
  //     this.bIsLoading = false;
  //   });
  // }

  trackByLabel(index, item) {
    if (!(index % 2)) {
      return item;
    }
  }

  getIDQR(nIdQr: number): string {
    return String(nIdQr);
  }

  sanitizer(url) {
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
}
