import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { QrService } from 'src/app/services/qr/qr.service';
import { ILines } from 'src/app/models/lines';
import { IFilesQR } from 'src/app/models/filesQr';
declare let $: any;
@Component({
  selector: 'ag-qr-list',
  templateUrl: './qr-list.component.html',
  styleUrls: ['./qr-list.component.scss']
})
export class QrListComponent implements OnInit {
  //@ViewChild('table') private table: ElementRef;
  [x: string]: any;
  search: any;
  page: number = 1;
  noReg = 9;
  files: IFilesQR[] = [];
  constructor(private _router: Router, private _qrService: QrService, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.getFiles();
  }

  getFiles() {
    this._qrService.getFiles()
      .then((data: any) => {
        this.files = data;
        if (this.files.length > 0)
          this.initTable();
      })
  }

  ngAfterViewInit() {

  }

  initTable() {

    this.cd.detectChanges();
    $(this.table.nativeElement).collapsible();

  }

}
