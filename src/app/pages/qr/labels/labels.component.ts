import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import LabelsGenerator from "@e-bitware/labelsgenerator";
import { fabric } from "fabric";
import { Label } from "src/app/models/label";
import { QrService } from "src/app/services/qr/qr.service";
import { AlertsService } from "src/app/services/shared/alerts.service";

@Component({
  selector: "ag-labels",
  templateUrl: "./labels.component.html",
  styleUrls: ["./labels.component.scss"]
})
export class LabelsComponent implements OnInit {
  img: Object;
  labelActive: Label = new Label();
  @ViewChild("imgg") imgg: ElementRef;
  one: fabric.IText;
  two: fabric.IText;
  three: fabric.IText;
  four: fabric.IText = new fabric.IText("5555555555  01/01/2000", {
    fontSize: 20,
    left: 30,
    top: 150,
    textBackgroundColor: "rgb(0,0,0,0.5)"
  });

  qr: fabric.Image;
  labels: Label[] = [];
  fonts: string[] = [
    "Monaco",
    "Georgia",
    "Times",
    "serif",
    "Helvética",
    "Garamond",
    "Verdana",
    "Georgia",
    "Bodoni",
    "Frutiger",
    "Trebuchet"
  ];
  fontSize: number[] = [
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30
  ];
  label: LabelsGenerator;
  constructor(private qrService: QrService, private _alert: AlertsService) {}
  async ngOnInit() {
    this.generateLabel();
    try {
      let res = await this.qrService.getLabels();
      console.log(res);
      this.labels = res.map(l => {
        return { ...l, data: JSON.parse(l.data) };
      }) as Label[];
    } catch (error) {
      this._alert.info({
        text: "Ocurrió un error al obtener los diseños de los productos."
      });
    }
  }

  goActive(idLabel: number) {
    const l = this.labels.find(l => l.id == idLabel);
    if (l) {
      this.generateLabel(l);
      this.labelActive = Object.assign({}, l);
    } else {
      this.labelActive = new Label();
      this.labelActive.id = null;
      this.generateLabel();
    }
  }
  selectLabel(idLabel: number) {
    let label = this.labels.find(l => l.id == idLabel);
    if (!label) return;

    this.generateLabel(label);
  }
  generateLabel(label?: Label) {
    if (!this.label) {
      this.label = new LabelsGenerator(
        [],
        "box",
        "fonts",
        "bold",
        "italic",
        "underline",
        "backround",
        "fontSize",
        ["lighten-4"]
      );
    } else {
      this.label.canvas._objects = [];
    }
    if (label) {
      this.one = new fabric.IText(label.data.one.text, label.data.one);
      this.two = new fabric.IText(label.data.two.text, label.data.two);
      this.three = new fabric.IText(label.data.three.text, label.data.three);
      this.four = new fabric.IText("5555555555  01/01/2000", label.data.four);
      this.qr = new fabric.Image(this.imgg.nativeElement, label.data.qr);
    } else {
      this.labelActive = new Label();
      this.labelActive.id = null;
      this.one = new fabric.IText("contenedor 1", {
        fontSize: 20,
        left: 20,
        top: 20
      });
      this.two = new fabric.IText("contenedor 2", {
        fontSize: 20,
        left: 200,
        top: 20
      });
      this.three = new fabric.IText("contenedor 3", {
        fontSize: 20,
        left: 20,
        top: 100
      });
      this.four = new fabric.IText("5555555555  01/01/2000", {
        fontSize: 20,
        left: 30,
        top: 150,
        textBackgroundColor: "rgb(0,0,0,0.5)"
      });
      this.qr = new fabric.Image(this.imgg.nativeElement, {
        left: 270,
        top: 60,
        width: 100,
        height: 100,
      });
      // this.label = new LabelsGenerator([this.one, this.two, this.three, this.four, this.qr], 'box', 'fonts', 'bold', 'italic', 'underline', 'backround', ['lighten-4'])
    }
    this.label.canvas.add(this.one, this.two, this.three, this.four, this.qr);
    // this.label = new LabelsGenerator( 'box', 'fonts', 'bold', 'italic', 'underline', 'backround', ['lighten-4'])
    // this.label.canvas.renderAll()
  }

  generateContainer(container: any) {
    let c = {
      width: container.width,
      top: container.top,
      left: container.left,
      height: container.height,
      scaleX: container.scaleX,
      scaleY: container.scaleY,
      fontSize: container.fontSize,
      fontFamily: container.fontFamily,
      fontStyle: container.fontStyle,
      fontWeight: container.fontWeight,
      angle: container.angle,
      deltaY: container.deltaY,
      type: container.type,
      deltaX: container.deltaX,
      fill: container.fill,
      textBackgroundColor: container.textBackgroundColor
    };
    if (container.type == "image") {
      c = { ...c, src: container.src } as any;
    }
    return c;
  }
  onDelete(id: number) {
    this._alert.question({
      text: "¿Estás seguro que deseas eliminar esta etiqueta?",
      showCancelButton: true
    }).then(result => {
      console.log(result);
      if (result.value){ 
        this.qrService.deleteLabels(id).then(() => {
          const index: number = this.labels.findIndex(x => x.id === id);
          this.labels.splice(index, 1);
          this.generateLabel();
        }).catch((err) => {
          this._alert.error({ text: err.message, title: "Ocurrió un error" });
        });
       
      }else{
        return;
      }

    });
  }
  async onSubmit(form: NgForm) {
    if (!form.valid) return this._alert.error({ text: "Verifica el nombre de la etiqueta" });
    if (form.value.name.length > 60)
      return this._alert.error({
        text: "Sólo puedes colocar 60 caracteres en el nombre"
      });
    // console.log(form);
    // console.log({ ...form.value, data: this.label.canvas._objects });
    // return
    let data = {
      one: this.one,
      two: this.two,
      three: this.three,
      four: this.four,
      qr: this.qr
    };
    if (this.labelActive.id) {
      let label = await this.qrService.patchLabels(this.labelActive.id, {
        name: form.value.name,
        data,
        active: true
      });
      this.labels = this.labels.filter(l => l.id != this.labelActive.id);
      this.labels.push(label as Label);
      this._alert.success({ text: "Guardado correctamente" });
    } else {
      let res = await this.qrService.postLabels({
        name: form.value.name,
        data: data,
        active: true
      });
      this.labels.push(res);
      this._alert.success({ text: "Guardado correctamente" });
    }
    this.generateLabel();
  }
}
