import { Component, OnInit } from "@angular/core";
import { FileUpload } from "src/app/models/fileUpload";
import { Util } from "src/app/common/util";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth.service";
import { AlertsService } from "src/app/services/shared/alerts.service";
import { slide } from "src/app/animations/slide";
import { QrService } from "src/app/services/qr/qr.service";

declare let $: any;

@Component({
  selector: "ag-qr-load-file",
  templateUrl: "./qr-load-file.component.html",
  styleUrls: ["./qr-load-file.component.scss"],
  animations: [slide]
})
export class QrLoadFileComponent implements OnInit {
  bIsLoading: boolean = true;
  isLoading: boolean = false;
  submitted: boolean = false;
  files: FileUpload[] = [];
  lstTypes: string[] = ["xls", "xlsx"];
  sTypesAccept: string = ".xls,.xlsx";
  isActions: number = 0;
  idArchivo: number;
  sTitle: string;
  registerLines: any[] = [];
  recargas: any[] = [];

  constructor(
    private _util: Util,
    private _auth: AuthService,
    private router: Router,
    private _alert: AlertsService,
    private _qrService: QrService
  ) {}

  ngOnInit() {
    this.bIsLoading = false;
  }

  displayFiles(event: any): void {
    this.isActions = 3;
    this._util.displayFile(event).then(
      (data: any) => {
        if (this.files.length > 0) {
          this.files = [];
        }

        this.files.push(data);
        $("#file-upload").val("");
        this.isActions = 4;
      },
      err => {
        this._alert.error({ text: err.message });
        this.isActions = 0;
      }
    );
  }

  dropFiles(event: any): void {
    this.isActions = 2;
    this._util.dropHandler(event, this.lstTypes).then(
      data => {
        let files = [];
        for (let item of data) {
          files.push(item.data);
        }
        this.displayFiles({ target: { files: files } });
      },
      err => {
        this._alert.error({ text: err.message });
        this.isActions = 0;
      }
    );
  }

  dragoverFile(event: any): void {
    this.isActions = 1;
    this._util.dragoverHandler(event);
  }

  dragEnd(event: any) {
    this.isActions = 0;
    this._util.dragendHandler(event);
  }

  removeFiles(index?: number): void {
    this.isActions = 0;
    this.files = [];
  }

  async sendFile() {
    this.bIsLoading = true;

    var formData = new FormData();
    formData.append("file", this.files[0].file);

    await this.postFile(formData);

    if (this.registerLines.length > 0) {
      $(document).ready(() => {
        const modal = $('#registros-duplicados');

        modal.modal({
          dismissible: false
        });
        modal.modal('open');

        $('#checkAll').on('click', function () {
          $('input:checkbox').not(this).prop('checked', this.checked);
        });

      });
    } else {
      this.router.navigate(['/qr/' + this.idArchivo]);
    }
    this.bIsLoading = false;

  }

  async postFile(form) {

    await this._qrService.postFile(form)
      .then((res: any) => {
        console.log(res);

        this.sTitle = res.message;
        this.registerLines = res.duplicados;
        this.recargas = res.recargas;
        this.idArchivo = res.idArchivo;

      }).catch((err: any) => {
        this.bIsLoading = false;
        this._alert.error({ title: 'Error al cargar', text: err });

      });
  }

  alertaLineasRegistradas() {
    let stringTable = "";
    this.registerLines.forEach(lin => {
      stringTable += `<tr> <td> ${lin.imei} </td> <td> ${lin.iccid} </td>  </tr>`;
    });
    this.bIsLoading = false;

    this._alert
      .warning({
        showCancelButton: true,
        title: this.sTitle,
        html: `<table>            <thead>            <tr>
      <th>IMEI</th>            <th>ICCID</th>            </td>
      </thead>            <tbody>            ${stringTable}            </tbody>            </table>`
        // showCloseButton: true,
      })
      .then((result: any) => {
        console.log(result);
        if (result.value) {
          this.sendConfirm(true);
        }
        if (result.dismiss == "cancel") {
          this.sendConfirm(false);
        }
      });
  }

  sendConfirm(bFlag: boolean) {
    if (bFlag) {
      var request = [];

      const checkeds = $("input:checked");

      $.each(checkeds, function(key, value) {
        if ($(value).val() != 0) {
          request.push({ iccid: $(value).val() });
        }
      });

      console.log("Duplicados: ", request);
      console.log("Recargas: ", this.recargas);

      if (this.recargas !== undefined && this.recargas !== null) {
        const r = this.recargas.reduce(
          (result, recarga) => result.concat({ iccid: recarga.iccid }),
          []
        );
        request = request.concat(r);
      }


      this._qrService
        .qrConfirmFile(request, this.idArchivo)
        .then((data: any) => {
          console.log(data);

          this.router.navigate(["/qr/" + this.idArchivo]);
        })
        .catch((err: any) => {
          this._alert.error({ title: "Error", text: err.responseMessage });
        });
    } else {
      return;
    }
  }
}
