import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LabelsComponent } from 'src/app/pages/qr/labels/labels.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from '../../pipes/pipes.module';
import { QrDetailComponent } from './qr-detail/qr-detail.component';
import { QrListComponent } from './qr-list/qr-list.component';
import { QrLoadFileComponent } from './qr-load-file/qr-load-file.component';
import { ROUTES_QR } from './qr.routing';
import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
  imports: [
    CommonModule,
    ROUTES_QR,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    SharedModule,
    NgxPaginationModule
  ],
  declarations: [QrListComponent, QrLoadFileComponent, QrDetailComponent, LabelsComponent]
})
export class QrModule { }
