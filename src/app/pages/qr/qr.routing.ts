import { RouterModule, Routes } from '@angular/router';
import { LabelsComponent } from 'src/app/pages/qr/labels/labels.component';
import { LoginGuard } from 'src/app/services/guards/login.guard';
// Components
import { PagesComponent } from '../pages.component';
import { QrDetailComponent } from './qr-detail/qr-detail.component';
import { QrListComponent } from './qr-list/qr-list.component';
import { QrLoadFileComponent } from './qr-load-file/qr-load-file.component';


const ROUTES: Routes = [
    {
        path: 'qr',
        component: PagesComponent,
        canActivate: [LoginGuard],
        children: [
            { path: '', component: QrListComponent },
            { path: 'loadFile', component: QrLoadFileComponent },
            { path: 'labels', component: LabelsComponent },
            { path: ':id', component: QrDetailComponent }
        ]
    }
]

export const ROUTES_QR = RouterModule.forChild(ROUTES);