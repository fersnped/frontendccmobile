import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { AuthService } from 'src/app/services/auth/auth.service';
import { BannersService } from 'src/app/services/banners/banners.service';
import { Banners, IBanners } from 'src/app/models/banners';

declare let $: any;


@Component({
	selector: 'ag-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
	@ViewChild("carousel") private carousel: ElementRef;

	fullName: string = '';
	banners: Banners[] = [];
	intervalCarousel: any = null;

	constructor(private _auth: AuthService, private _bannerService: BannersService, private cd: ChangeDetectorRef) {
		this.getBanners();
	}


	ngOnInit() {
		this.cd.detectChanges();
		let user = this._auth.getUser();
		this.fullName = `${user.nombre} ${user.apellido_Paterno} ${user.apellido_Materno}`;

	}

	ngAfterViewInit() {
	}




	getBanners() {
		this._bannerService.getBanners()
			.then((data: any) => {
				this.banners = data;

				if (this.banners.length > 0) {

					setTimeout(() => {
						$(this.carousel.nativeElement).carousel({
							indicators: true,
							fullWidth: true,
							padding: 20,
							duration: 1000,
						});
						$(this.carousel.nativeElement).carousel('set', 0);

						this.intervalCarousel = setInterval(() => {
							$(this.carousel.nativeElement).carousel('next');
						}, 15000);
					}, 1000);
				}
			})
	}


	clickImage(event: Event) {
		event.preventDefault();
		clearInterval(this.intervalCarousel)
		this.intervalCarousel = setInterval(() => {
			$(this.carousel.nativeElement).carousel('next');
		}, 15000);
	}

}
