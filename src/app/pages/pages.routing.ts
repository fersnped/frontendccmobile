import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { PagesComponent } from "./pages.component";
import { LoginGuard } from "../services/guards/login.guard";
import { SaleComponent } from "./sale/sale.component";
import { NewSaleComponent } from './sale/newSales/newSales.component'

const ROUTES: Routes = [
  {
    path: "home",
    component: PagesComponent,
    canActivate: [LoginGuard],
    children: [{ path: "", component: HomeComponent }]
  },
  {
    path: "sale",
    component: PagesComponent,
    canActivate: [LoginGuard],
    children: [
      { path: "", component: SaleComponent },
      { path: ":id", component: NewSaleComponent }
    ]
  }
];

export const ROUTES_PAGES = RouterModule.forChild(ROUTES);
