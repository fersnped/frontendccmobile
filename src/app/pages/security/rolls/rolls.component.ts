import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RollsService } from '../../../services/rolls/rolls.service';
import { AlertsService } from '../../../services/shared/alerts.service';
import { IRoll } from 'src/app/models/roll';


declare let $: any;
@Component({
	selector: 'ag-rolls',
	templateUrl: './rolls.component.html',
	styleUrls: ['./rolls.component.scss']
})
export class RollsComponent implements OnInit {
	@ViewChild('table') private table: ElementRef;
	users: any[] = [];
	search: any;
	noReg = 10;
	page: number = 1;
	rolls: IRoll[] = [];

	constructor(
		private _rollsService: RollsService,
		private _alert: AlertsService
	) { }

	ngOnInit() {
		this.fetchRolls();
	}

	ngAfterViewInit() {
		$(this.table.nativeElement).collapsible();
	}

	fetchRolls() {
		this._rollsService.get()
			.then((res: any) => {
				this.rolls = res;
			});
	}

	deleteRoll(id: number) {
		this._alert.question({
			text: '¿Estás seguro que deseas eliminar este rol?',
			showCancelButton: true
		}).then((result) => {
			if (result.value)
				return this._rollsService.delete(id);
		}).then((res) => {
			return this._alert.success({ text: res.message })
		}, (err) => {
			this._alert.error({ text: err.message, title: 'Ocurrio un error' });
		}).then(() => {
			const index: number = this.rolls.findIndex(x => x.id === id);
			this.rolls.splice(index, 1);
		});
	}

	toggleState(index: number) {
		const roll: IRoll = this.rolls[index];

		this._alert.question({
			text: `¿Estás seguro que deseas ${roll.activo ? 'bloquear' : 'desbloquear'} este rol?`,
			showCancelButton: true
		}).then((result) => {
			if (result.value)
				return this._rollsService.toggleActive(roll.id)
		}).then((res) => {
			roll.activo = !roll.activo;
			this._alert.success({ text: res.message });
		}, (err) => {
			this._alert.error({ text: err.message, title: 'Ocurrio un error' });
		});
	}

	trackByFn(index: number, item: any) {
		return item.id
	}

}
