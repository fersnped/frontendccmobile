import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RollsService } from '../../../../services/rolls/rolls.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from '../../../../services/shared/alerts.service';
import { LoaderService } from '../../../../services/shared/loader.service';
import { ModulesService } from 'src/app/services/modules/modules.service';
import { IRoll, Roll } from 'src/app/models/roll';
import { IModule } from 'src/app/models/module';

@Component({
	selector: 'ag-rolls-details',
	templateUrl: './rolls-details.component.html',
	styleUrls: ['./rolls-details.component.scss']
})
export class RollsDetailsComponent implements OnInit {
	form: FormGroup;
	roll: IRoll = new Roll();
	modules: IModule[] = [];
	isNew: boolean = true;

	idRol: number;

	constructor(
		private _activatedRoute: ActivatedRoute,
		private _rollsService: RollsService,
		private _modulesService: ModulesService,
		private _fb: FormBuilder,
		private _alert: AlertsService,
		private _router: Router,
		private _loader: LoaderService
	) { }

	ngOnInit() {
		this.buildForm();

		this._activatedRoute.paramMap
			.subscribe((params: any) => {
				this.idRol = parseInt(params.get('id'))
				if (this.idRol) {
					this.fetchRoll(this.idRol)
						.then((roll: any) => {
							this.roll = roll;
							this.roll.modulos.forEach(mod1 => {
								this.roll.modulos.forEach(mod2 => {
									if (mod1.id == mod2.id)
										mod1.tipo_permiso = mod2.permiso
								})
							})
							this.isNew = this.roll.id ? false : true;
						})
				}
				this.fetchModules()
					.then((modules: any) => {
						this.modules = modules;
					});
			})
	}

	buildForm() {
		this.form = this._fb.group({
			nombre: [this.roll.nombre, Validators.required],
			descripcion: [this.roll.descripcion, Validators.required]
		});
	}

	fetchRoll(id: number): Promise<any> {
		if (!id) return Promise.resolve({});

		return this._rollsService.getById(id)
			.then((roll) => {
				return roll;
			});
	}

	fetchModules(): Promise<any> {
		return this._modulesService.get()
			.then((modules) => {
				return modules;
			});
	}

	registerRoll() {
		console.log(this.roll)
		if (this.isNew) {
			console.log("POST")
			this._rollsService.create(this.roll)
				.then((res) => {
					this._loader.hide();
					return this._alert.success({ text: 'El rol ha sido creado exitosamente' });
				}, (err) => {
					this._loader.hide();
					this._alert.error({ text: err.message });
				}).then(() => {
					this._router.navigate(['..'], { relativeTo: this._activatedRoute });
				});
		} else {
			console.log("PUT")
			this._rollsService.update(this.roll.id, this.roll)
				.then((res) => {
					this._loader.hide();
					return this._alert.success({ text: 'El rol ha sido actualizado exitosamente' })
				}, (err) => {
					this._loader.hide();
					this._alert.error({ text: err.message });
				}).then(() => {
					this._router.navigate(['..'], { relativeTo: this._activatedRoute });
				});
		}
	}

	checkPermisos(idModulo: number, nPermiso: number): boolean {

		if (this.roll.modulos != null) {
			if (this.roll.modulos.length > 0) {
				if (this.roll.modulos.find(x => x.id == idModulo && x.tipo_permiso == nPermiso))
					return true;
				else
					return false;
			}
		}

	}

	addModule(idModulo, nPermiso) {
		if (!this.roll.modulos) this.roll.modulos = [];
		if (this.isInRoles(idModulo)) {
			for (let i = 0; i < this.roll.modulos.length; i++) {
				if (this.roll.modulos[i].id == idModulo && this.roll.modulos[i].tipo_permiso == nPermiso) {
					this.roll.modulos.splice(i, 1);
					break;
				}
				if (this.roll.modulos[i].id == idModulo && this.roll.modulos[i].tipo_permiso != nPermiso) {
					this.roll.modulos[i].tipo_permiso = nPermiso;
					break;
				}
			}
		} else {
			this.roll.modulos.push({ id: idModulo, tipo_permiso: nPermiso });
		}
	}

	isInRoles(idModulo: number): boolean {
		let flag = false;
		if (!this.roll.modulos || this.roll.modulos.length < 1) return false;
		for (let i = 0; i < this.roll.modulos.length; i++) {
			if (this.roll.modulos[i].id === idModulo) {
				flag = true;
				break;
			}
		}
		return flag;
	}


}
