import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivatekitComponent } from './activatekit.component';

describe('ActivatekitComponent', () => {
  let component: ActivatekitComponent;
  let fixture: ComponentFixture<ActivatekitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivatekitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivatekitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
