import { FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ActivationService } from '../../../../services/activation/activation.service';
import { IImei } from '../../../../models/imei';
import { IIccid } from '../../../../models/iccid';
import { ICiudad } from '../../../../models/ciudad';
import { IProducto } from '../../../../models/producto';
import { IPlan } from '../../../../models/plan';
import { Router } from '@angular/router';
import { ActivationConfirm, IActivationConfirm } from '../../../../models/activationConfirm';
import { ActivateKit, IActivateKit } from '../../../../models/activateKit';
import { Catalogo, ICatalogo } from '../../../../models/catalogo';
import { CURRENT_USER } from '../../../../config/config';

declare var $: any;

@Component({
  selector: 'ag-activatekit',
  templateUrl: './activatekit.component.html',
  styleUrls: ['./activatekit.component.scss']
})
export class ActivatekitComponent implements OnInit, AfterViewInit {

  @ViewChild("inputPlanes") private inputPlanes: ElementRef
  @ViewChild("inputCiudades") private inputCiudades: ElementRef


  imei: IImei;
  iccid: IIccid;
  private numberPattern = /^[0-9,$]*$/;
  producto = 1;
  ciudades: ICiudad[];
  planes: IPlan[];
  catalogos: ICatalogo = new Catalogo();
  model: IActivateKit = new ActivateKit();

  confirmacion: IActivationConfirm = new ActivationConfirm();


  constructor(private activationService: ActivationService, private router: Router, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getCatalogos();
    this.getCiudades();
    this.model.plan = 0;
    this.model.cuidad = 0;

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.initComponets();
    }, 1000);
  }

  async process() {
    // console.log('Process...', this.model);
    this.confirmacion.producto = this.catalogos.productos.find(prod => prod.id == this.model.producto);
    this.confirmacion.plan = this.planes.find(pl => pl.id == this.model.plan);
    this.confirmacion.tarifa = this.catalogos.tarifas.find(t => t.id == this.model.tarifa);
    this.confirmacion.ciudad = this.ciudades.find(c => c.id == this.model.cuidad);
    this.confirmacion.imei = this.model.imei;
    this.confirmacion.iccid = this.model.icci;

    this.activationService.changeConfirmacion(this.confirmacion);

    localStorage.setItem('CONFIRMACION', JSON.stringify(this.confirmacion));

    this.router.navigate(['security/activation/confirmacion']);
  }

  changePlan() {
    console.log(this.model.producto)
    const producto: IProducto = this.catalogos.productos.find(prod => prod.id == this.model.producto);
    this.model.imei = undefined;
    this.model.icci = undefined;

    this.planes = [];
    this.planes = producto.planes;

    console.log(this.planes)
    if (producto.id !== this.catalogos.productos[2].id) {
      this.model.tarifa = this.catalogos.tarifas[0].id;
    }
    if (producto.id === this.catalogos.productos[2].id) {
      this.model.tarifa = undefined;
    }
    setTimeout(() => {
      this.initComponets();
    }, 500);
  }

  getCiudades(): void {

    this.activationService.getCiudades()
      .then(res => {
        // this.customSelect();
        this.ciudades = res;
      });
  }

  getCatalogos(): void {

    this.activationService.getCatalogos()
      .then(res => {
        this.catalogos = res;
        this.model.producto = res.productos[0].id;
        this.model.tarifa = res.tarifas[0].id;
        this.planes = res.productos[0].planes;
      });
  }

  async validateImei(imei: string) {

    if (imei !== '') {
      await this.activationService.validateImei(imei)
        .then(res => {
          console.log('Response en validate imei', res);
          this.imei = res;
        });
    }
  }


  async validateIccid(iccid: string) {
    if (iccid !== '') {
      await this.activationService.validateIccid(iccid)
        .then(res => {
          this.iccid = res;
        });

    }

  }

  numberOnly(event) {
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !this.numberPattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  // customSelect() {

  //   $(document).ready(() => {

  //     const select = $('select');
  //     select.formSelect();
  //     // $('.activation-kit input').focus(function () {
  //     //   $(this).css('border-bottom', '1px solid #3E5F89');
  //     // });

  //     // $('.activation-kit .dropdown-content li > a').css('color', '#3E5F89');
  //     // $('.activation-kit .dropdown-content li > span').css('color', '#3E5F89');
  //     // $('.activation-kit .select-dropdown').css({ 'color': '#bbbbbb', 'text-align': 'center' });
  //   });
  // }

  initComponets() {
    $(this.inputCiudades.nativeElement).formSelect();
    $(this.inputPlanes.nativeElement).formSelect();
    $('.activation-kit .select-dropdown').css({ 'color': '#bbbbbb', 'text-align': 'center', 'max-height': '250px' });
  }
}
