import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActivationDetail, IActivationDetail } from '../../../../models/activationDetail';
import { ActivationService } from '../../../../services/activation/activation.service';
import { AlertsService } from 'src/app/services/shared/alerts.service';


@Component({
  selector: 'ag-detalle',
  templateUrl: './activacion-detalle.component.html',
  styleUrls: ['./activacion-detalle.component.css']
})
export class ActivacionDetalleComponent implements OnInit {

  folio: string;
  detail: IActivationDetail = new ActivationDetail();

  constructor(private route: ActivatedRoute, private activationService: ActivationService, private _alert: AlertsService) {
  }

  ngOnInit() {
    this.folio = this.route.snapshot.paramMap.get('folio');

    this.getActivationsDetail();

  }

  getActivationsDetail(): void {
    this.activationService.getActivationByFolio(this.folio)
      .then(res => {
        this.detail = res;
        console.log(this.detail)
      });
  }

  recargarLinea() {
    this.activationService.recargarLinea(this.folio)
      .then((res: any) => {
        this._alert.success({ title: "Exito!!", text: res })
          .then((res) => {
            this.getActivationsDetail();
          })
      })
      .catch((res) => {
        this._alert.error({ title: "¡Error!", text: res })
          .then((res) => {
            this.getActivationsDetail();
          })
      })
  }

}
