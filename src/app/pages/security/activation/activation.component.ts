import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivationService} from '../../../services/activation/activation.service';
import {AuthService} from '../../../services/auth/auth.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {IActivation} from '../../../models/activation';
import {Router} from '@angular/router';

@Component({
  selector: 'ag-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.scss']
})
export class ActivationComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['Folio', 'IMEI', 'ICCID', 'TipoActivacion', 'Numero', 'Estatus','Acciones'];
  dataSource: MatTableDataSource<IActivation>;
  activations: any = [];
  isEmptyList = false;

  constructor(private activationService: ActivationService,
              private auth: AuthService,
              private router: Router) {

    console.log('Constructor');
  }

  ngOnInit() {
    console.log('Init');
    this.getAllActivations();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  getAllActivations(): void {

    this.activationService.getAll(this.auth.getUser().id)
      .then(res => {

        this.isEmptyList = res.length <= 0;
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.activations = res;

      }, (err) => {
        console.log(err);
      });
  }

  showDetail(row: IActivation) {
    this.router.navigate(['security/activation/detalle', row.Folio]);
    console.log(row);
  }
}
