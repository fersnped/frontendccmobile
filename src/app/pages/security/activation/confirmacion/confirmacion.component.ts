import { Component, OnInit } from '@angular/core';
import { ActivationConfirm, IActivationConfirm } from '../../../../models/activationConfirm';
import { ActivationService } from '../../../../services/activation/activation.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../../services/auth/auth.service';
import { ActivationRequest, IActivationRequest } from '../../../../models/request/activation';
import { IUser } from '../../../../models/user';
import { AppInfo, OSVersions } from 'src/app/models/appInfo';
import { DeviceDetectorService } from 'ngx-device-detector';

const CONFIRMACION_KEY = 'CONFIRMACION';

@Component({
  selector: 'ag-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.css']
})
export class ConfirmacionComponent implements OnInit {

  confirmacion: IActivationConfirm;
  request: IActivationRequest = new ActivationRequest();
  user: IUser;


  constructor(private activationService: ActivationService,
    private router: Router, private auth: AuthService,
    private deviceService: DeviceDetectorService) {
    this.activationService.currentConfirmacion.subscribe((confirmacion) => {

      const lsConfirmacion = JSON.parse(localStorage.getItem(CONFIRMACION_KEY));

      if (!lsConfirmacion) {
        this.cancelar();
      }

      const isCurrent = Object.is(confirmacion, lsConfirmacion);

      if (isCurrent) {
        this.confirmacion = confirmacion;
      } else {
        this.confirmacion = lsConfirmacion;
      }
    });
  }

  ngOnInit() {
    this.user = this.auth.getUser();
  }

  crearActivacion(): void {

    localStorage.removeItem(CONFIRMACION_KEY);

    this.request.usuarioId = this.user.id.toString();
    this.request.productoId = this.confirmacion.producto.id.toString();
    this.request.planId = this.confirmacion.plan.id.toString();
    this.request.iccid = this.confirmacion.iccid;
    this.request.imei = this.confirmacion.imei;
    this.request.ciudadId = this.confirmacion.ciudad.id.toString();

    this.request.appInfo = {
      plataforma: 1,
      marcaOnavegador: this.deviceService.browser,
      modeloOversionNavegador: this.deviceService.browser_version,
      os: this.getOS(this.deviceService.os),
      versionApp: "0.0",
      versionOs: "0.0"
    }

    if (this.confirmacion.tarifa) {
      this.request.tarifaId = this.confirmacion.tarifa.id.toString();
    }
    // console.log(this.request)
    this.activationService.crearActivacion(this.request)
      .then(res => {
        console.log(res);
        this.router.navigate(['security/activation/detalle', res.Folio]);
      });
  }

  cancelar() {
    this.router.navigate(['security/activation/activatekit']);
  }


  getOS(osName: string): number {
    let nIdOs
    switch (osName) {
      case 'iOS':
        nIdOs = OSVersions.iOS;
        break;
      case 'Android':
        nIdOs = OSVersions.Android;
        break;
      case 'Linux':
        nIdOs = OSVersions.Linux;
        break;
      case 'Windows':
        nIdOs = OSVersions.Windows;
        break;
      case 'MacOs':
        nIdOs = OSVersions.MacOs;
        break;
    }
    return nIdOs;
  }

}
