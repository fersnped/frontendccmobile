import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ActivationService} from '../../../../services/activation/activation.service';
import {ISubdistribuidor} from '../../../../models/subdistribuidor';
import {AuthService} from '../../../../services/auth/auth.service';
import {IUser} from '../../../../models/user';
import {IModule} from '../../../../models/module';

declare var $: any;

@Component({
  selector: 'ag-activacion-new',
  templateUrl: './activacion-new.component.html',
  styleUrls: ['./activacion-new.component.css']
})
export class ActivacionNewComponent implements OnInit {

  distribuidores: ISubdistribuidor[];
  user: IUser;
  callCenter: IModule;

  constructor(private router: Router, private activationService: ActivationService, private auth: AuthService) {
    this.user = auth.getUser();

    this.callCenter = this.user.modulos.find(callCenter => callCenter.nombre === 'Call Center');
  }

  ngOnInit() {
    this.getAllActivations();
  }

  goActivacion() {
    this.router.navigate(['security/activation/activatekit']);
  }


  getAllActivations(): void {

    this.activationService.getDistribuidores()
      .then(res => {
        console.log(res);
        this.distribuidores = res;
        this.customSelect();
      });
  }

  async customSelect() {

    $(document).ready(() => {

      const select = $('select');
      select.formSelect();
      $('.subdistribuidor input').focus(function () {
        $(this).css('border-bottom', '1px solid #3E5F89');
      });

      $('.subdistribuidor .dropdown-content li > a').css('color', '#3E5F89');
      $('.subdistribuidor .dropdown-content li > span').css('color', '#3E5F89');
      $('.subdistribuidor .select-dropdown').css({'width': '400px', 'color': '#bbbbbb', 'text-align': 'center'});
    });
  }

}
