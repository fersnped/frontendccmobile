import { Component, OnInit } from "@angular/core";
import { RecargasService } from "../../../../services/recargas/recargas.service";
import {
  IRecargasDisponibles,
  IRecargasRealizadas
} from "../../../../models/recargasRealizadas";
import { ExcelService } from "../../../../services/excel.service";
import { AlertsService } from "src/app/services/shared/alerts.service";
declare var $: any;

@Component({
  selector: "ag-reportes",
  templateUrl: "./reportes.component.html",
  styleUrls: ["./reportes.component.scss"]
})
export class ReportesComponent implements OnInit {
  tipoRecarga = 1;
  fechaInicio: any;
  fechaFin: any;
  recargasDisponibles: IRecargasDisponibles[];
  recargasRealizadas: IRecargasRealizadas[];
  numero: string = "";
  iccid: string = "";
  nombre_usuario: string = "";
  noReg = 10;
  page: number = 1;
  options = {
    format: "yyyy-mm-dd",
    maxDate: new Date(),
    autoClose: true,
    showClearBtn: true
  };
  petition: boolean = false;

  constructor(
    private recargasService: RecargasService,
    private _alert: AlertsService,
    private _excel: ExcelService
  ) { }

  ngOnInit() {
    this.customSelect();
    this.fechaInicio = $("#fechaInicio").datepicker(this.options);
    this.fechaFin = $("#fechaFin").datepicker(this.options);
  }

  sendFilter() {
    this.page=1;
    const dateInicio = this.fechaInicio.val();
    const dateFin = this.fechaFin.val();
    this.customSelect();

    if (this.tipoRecarga == 1) {
      this.getRecargasDisponibles(dateInicio, dateFin);
    } else {
      this.getRecargasRealizadas(dateInicio, dateFin);
    }
  }

  validationView() {
    if (this.tipoRecarga == 1) {
      if (
        !this.recargasDisponibles ||
        (this.recargasDisponibles && this.recargasDisponibles.length == 0)
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      if (
        !this.recargasRealizadas ||
        (this.recargasRealizadas && this.recargasRealizadas.length == 0)
      ) {
        return false;
      } else {
        return true;
      }
    }
  }
  async getRecargasDisponibles(fechaInicial: string, fechaFinal: string) {
    this.petition = true;
    try {
      this.recargasDisponibles = await this.recargasService.getRecargasDisponibles(
        fechaInicial,
        fechaFinal,
        this.numero,
        this.iccid,
        this.nombre_usuario
      );
      if (this.recargasDisponibles.length <= 0) {
        return this._alert.warning({ text: "No hay registros" });
      }
    } finally {
      this.petition = false;
    }
  }

  async getRecargasRealizadas(fechaInicial: string, fechaFinal: string) {
    this.petition = true;
    try {
      this.recargasRealizadas = await this.recargasService.getRecargasRealizadas(
        fechaInicial,
        fechaFinal,
        this.numero,
        this.iccid,
        this.nombre_usuario
      );
      if (this.recargasRealizadas.length <= 0) {
        return this._alert.warning({ text: "No hay registros" });
      }
      console.log(this.recargasDisponibles, this.recargasRealizadas);
    } finally {
      this.petition = false;
    }
  }

  async customSelect() {
    // $(document).ready(() => {
    //   $("select").formSelect();
    //   $(".reporte-recargas input").focus(function() {
    //     $(this).css("border-bottom", "1px solid #3E5F89");
    //   });
    //   $(".reporte-recargas .dropdown-content li > a").css("color", "#3E5F89");
    //   $(".reporte-recargas .dropdown-content li > span").css(
    //     "color",
    //     "#3E5F89"
    //   );
    //   $(".reporte-recargas .select-dropdown").css("color", "#bbbbbb");
    // });
  }
  async generateExcel() {
    console.log(this.recargasRealizadas)
    console.log("Sal")
    console.log(this.recargasDisponibles)
    let [headers, items] = await this.generateFormat();
    if (this.tipoRecarga == 2) {
      if (!this.recargasRealizadas) {
        return;
      }
      await this._excel.generateExcel(
        headers,
        items,
        `Reporte_recargas_realizadas`
      );
    } else {
      if (!this.recargasDisponibles) {
        return;
      }
      await this._excel.generateExcel(
        headers,
        items,
        `Reporte_recargas_disponibles`
      );
    }
  }
  async generateFormat(): Promise<[any, any]> {
    if (this.tipoRecarga == 2) {
      let headers = {
        id: "Identificador",
        numero: "Telefono",
        imei: "IMEI",
        iccid: "ICCID",
        recarga: "Recarga",
        producto: "Producto",
        fecha_activacion: "Fecha de activación",
        fecha_vigencia: "Fecha de vigencia",
        fecha_actualizacion: "Fecha de recarga",
        nombre_usuario: "Nombre del usuario",
        noExterior: "No",
        colonia: "Colonia",
        ciudad: "Cuidad",
        estado: "Estado",
        cp: "Codigo postal",
        pais: "Pais",
        calle: "Calle",
      };
      "str".normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      return [
        headers,
        this.transform(
          this.recargasRealizadas.map((v: any) => ({
            ...v,
            calle: v.calle.normalize("NFD").replace(/[\u0300-\u036f]/g, ""),
            ciudad: v.ciudad.normalize("NFD").replace(/[\u0300-\u036f]/g, ""),
            colonia: v.colonia.normalize("NFD").replace(/[\u0300-\u036f]/g, ""),
            estado: v.estado.normalize("NFD").replace(/[\u0300-\u036f]/g, ""),
            pais: v.pais.normalize("NFD").replace(/[\u0300-\u036f]/g, ""),
            iccid:
              `${v.iccid}'`
          })),

          {
            numero: this.numero,
            iccid: this.iccid,
            nombre_usuario: this.nombre_usuario
          }
        )
      ];
    } else {
      let headers = {
        id: "Identificador",
        numero: "Telefono",
        imei: "IMEI",
        iccid: "ICCID",
        recarga: "Recarga",
        producto: "Producto",
        fecha_activacion: "Fecha de activación",
        fecha_vigencia: "Fecha de vigencia"
      };
      return [
        headers,
        this.transform(
          this.recargasDisponibles.map((v: any) => ({
            ...v,
            iccid: `${v.iccid}'`
          })),

          {
            numero: this.numero,
            iccid: this.iccid,
            nombre_usuario: this.nombre_usuario
          }
        )
      ];
    }
  }
  transform(value: Array<any>, types): Array<any> {
    //arg[0] Busqueda
    //arg[1] Columnas que pertenecen a la busqueda
    if (!value || value.length <= 0) return [];
    let { numero, iccid, nombre_usuario } = types;
    if (!numero && !iccid && !nombre_usuario) return value;

    return value.filter(dato => {
      if (numero && iccid) {
        return (
          this.getIndexOf(dato, numero, "numero") >= 0 &&
          this.getIndexOf(dato, iccid, "iccid") >= 0
        );
      }
      if (numero && nombre_usuario) {
        return (
          this.getIndexOf(dato, numero, "numero") >= 0 &&
          this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0
        );
      }
      if (iccid && nombre_usuario) {
        return (
          this.getIndexOf(dato, iccid, "iccid") >= 0 &&
          this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0
        );
      }
      if (iccid && nombre_usuario && numero) {
        return (
          this.getIndexOf(dato, numero, "numero") >= 0 &&
          this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0 &&
          this.getIndexOf(dato, iccid, "iccid") >= 0
        );
      }
      return (
        this.getIndexOf(dato, numero, "numero") >= 0 ||
        this.getIndexOf(dato, nombre_usuario, "nombre_usuario") >= 0 ||
        this.getIndexOf(dato, iccid, "iccid") >= 0
      );
    });
  }
  getIndexOf(dato, other, type: string) {
    if (other) {
      return String(dato[type])
        .toLowerCase()
        .trim()
        .replace(" ", "")
        .replace("\t", "")
        .indexOf(other);
    } else {
      return -1;
    }
  }
}
