import { NgModule } from "@angular/core";
import { ReportesComponent } from "./reportes/reportes.component";
import { ROUTES_SECURITY_RECARGAS } from "./recargas.routing";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { PipesModule } from "../../../pipes/pipes.module";

@NgModule({
  declarations: [ReportesComponent],
  imports: [
    ROUTES_SECURITY_RECARGAS,
    BrowserModule,
    NgxPaginationModule,
    FormsModule,
    PipesModule
  ],
  exports: []
})
export class RecargasModule {}
