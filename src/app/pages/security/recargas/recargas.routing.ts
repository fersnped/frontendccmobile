import {Routes, RouterModule} from '@angular/router';
import {PagesComponent} from '../../pages.component';
import {LoginGuard} from '../../../services/guards/login.guard';
import {ReportesComponent} from './reportes/reportes.component';


const ROUTES: Routes = [
  {
    path: 'security/recargas',
    component: PagesComponent,
    canActivate: [LoginGuard],
    children: [
      {path: 'reportes', component: ReportesComponent},
    ]
  }
];

export const ROUTES_SECURITY_RECARGAS = RouterModule.forChild(ROUTES);
