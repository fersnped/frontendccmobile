import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../../../services/users/users.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { IUser, User } from '../../../../models/user';
import { AlertsService } from '../../../../services/shared/alerts.service';
import { CarriersService } from '../../../../services/carriers/carriers.service';
import { LoaderService } from '../../../../services/shared/loader.service';
import { RollsService } from '../../../../services/rolls/rolls.service';
import { ModulesService } from 'src/app/services/modules/modules.service';
import { IRoll, Roll } from 'src/app/models/roll';
import { ICarrier } from 'src/app/models/carrier';
import { IModule } from 'src/app/models/module';
import { IUserRequest, UserRequest } from 'src/app/models/request/users';
import { SalesService } from 'src/app/services/sales/sales.service';
import { IPerfil } from 'src/app/models/perfil';

declare let $: any;

@Component({
	selector: 'ag-users-details',
	templateUrl: './users-details.component.html',
	styleUrls: ['./users-details.component.scss']
})
export class UsersDetailsComponent implements OnInit {
	@ViewChild('carrier') private carrier: ElementRef;
	@ViewChild('rol') private rol: ElementRef;
	@ViewChild('typeUser') private typeUser: ElementRef;
	@ViewChild('selAgName') private selAgName: ElementRef;
	@ViewChild('profile') private profile: ElementRef;
	title: string;
	form: FormGroup;
	isNew: boolean = true;
	user: IUser = new User();
	agNameList: any[] = [];
	modules: IModule[] = [];
	carriers: ICarrier[] = [];
	rolls: IRoll[] = [];
	bIsLoading: boolean = true;
	idUsuario: number;
	userRequest: IUserRequest = new UserRequest();
	lstModAct: IModule[] = [];
	lsPerfiles: IPerfil[] = [];
	public checked: boolean ;

	constructor(
		private _activatedRoute: ActivatedRoute,
		private _usersService: UsersService,
		private _rollsService: RollsService,
		private _modulesService: ModulesService,
		private _carriersService: CarriersService,
		private _fb: FormBuilder,
		private _alert: AlertsService,
		private _router: Router,
		private _loader: LoaderService,
		private cd: ChangeDetectorRef,
		private _salesService: SalesService
	) {
		this.user.nombre_Usuario = '';
		this.checked = true;
	}

	ngOnInit() {
		this.bIsLoading = true;
		this.getProfiles();
		this._activatedRoute.paramMap.subscribe((params) => {
			this.idUsuario = parseInt(params.get("id"))
			this.fetchUser(this.idUsuario)
				.then((user) => {
					if (user) {
						this.user = user;
						this.user.rol.modulos.forEach(x => {
							this.lstModAct.push({ id: x.acceso_id, permiso: x.permiso })
						})
						this.user.rol.modulos = this.lstModAct
						console.log(this.user.rol.modulos)
					}
					else
						this.user = new User
					this.isNew = this.idUsuario ? false : true;
					return this.fetchRolls();
				})
				.then((rolls: any) => {
					this.rolls = rolls;
					return this.fetchModules();
				}).then((modules: IModule[]) => {
					this.modules = modules;
					return this.fetchCarriers();
				})
				.then((carries: any) => {
					this.carriers = carries;
					/*if (this.isNew) {
						return this.fetchNumInt(0)
					}
					else {
						return this.fetchNumInt(parseInt(this.user.typeUser))
					}
				}).then((agNum) => {
					this.agNameList = agNum;*/
					this.bIsLoading = false;
					if(params.get("id") != null){
						this.title = "Editar usuario";

					}else{
						this.title = "Crear usuario";
					}
          this.buildForm();
          this.initComponets()
				});
		})


	}

	ngAfterViewInit() {
		// setTimeout(() => {
		// 	this.initComponets();
		// }, 4000);

	}

	initComponets() {
		this.cd.detectChanges();
		$(this.carrier.nativeElement).formSelect();
		$(this.rol.nativeElement).formSelect();
		$(this.typeUser.nativeElement).formSelect();
		//$(this.selAgName.nativeElement).formSelect();
		$(this.profile.nativeElement).formSelect();

	}

	buildForm() {
		this.form = this._fb.group({
			nombre: [this.user.nombre, [Validators.required]],
			apellido_Paterno: [this.user.apellido_Paterno, [Validators.required]],
			apellido_Materno: [this.user.apellido_Materno, [Validators.required]],
			tipo: [this.user.tipo_persona, [Validators.required]],
			rfc: [this.user.rfc, [Validators.required]],
			telefono: [this.user.telefono, [Validators.required]],
			email: [this.user.correo, [Validators.required]],
			idRol: [this.user.rol.id, [Validators.required]],
			idCarrier: [this.user.carrier.id, [Validators.required]],
			razon_social: [this.user.razon_social, [Validators.required]],
			permisos_especiales: [false, []],
			typeUser: [this.user.typeUser, [Validators.required]],
			numeroInternoAGID: [this.user.numero_Interno_AG_ID, [Validators.required]],
			direccion: [this.user.direccion, [Validators.required]],
			idPerfil: [this.user.perfilId, [Validators.required]],
			nombreCompleto: this.user.nombre + " " + this.user.apellido_Paterno,
			
		});
		this.user.permisos_especiales = this.user.rol.id == -1 ? true : false;

	}

	fetchUser(id: number): Promise<IUser> {
		if (id) {
			return this._usersService.getById(id)
				.then((user) => {
					return user;
				});
		} else
			return Promise.resolve(null);
	}

	fetchModules(): Promise<IModule[]> {
		return this._modulesService.get()
			.then((modules) => {
				//console.log(modules)
				return modules;
			});
	}

	fetchRolls(): Promise<IRoll[]> {
		return this._rollsService.get()
			.then((rolls: any) => {
				// console.log(rolls)
				return rolls;
			});
	}

	fetchCarriers(): Promise<any[]> {
		return this._carriersService.get()
			.then((carrier) => {
				// console.log(carrier)
				return carrier;
			});
	}

	fetchNumInt(id: number = 0) {
		return this._salesService.getAgNames(id, 2)
			.then((res) => {
				return res
			});
	}

	/*changeTypeUser() {
		this.agNameList = []
		this.fetchNumInt(parseInt(this.user.typeUser))
			.then((agNum) => {
				this.agNameList = agNum.filter(x => x.usuarioId != "all");
				this.user.numero_Interno_AG_ID = this.agNameList[0].usuarioId
				this.initComponets();
				// this.cd.detectChanges();
			})
	}*/

	togglePerson(tipo) {
		this.user.tipo_persona = tipo;
	}
	togglePermisosEspeciales(){
		if(this.checked){
			this.checked = false;
		}
		else{
			this.checked = true;
		}
		console.log("daeda")
	}

	registerUser() {
		this._loader.show();
		this.castToModel();


		if (this.isNew) {
			this._usersService.create(this.userRequest)
				.then((res) => {
					this._loader.hide();
					return this._alert.success({ text: 'El usuario ha sido creado exitosamente' })
						.then((res) => {
							if (res.value == true)
								this._router.navigate(['..'], { relativeTo: this._activatedRoute });
						});
				}, (err) => {
					this._loader.hide();
					this._alert.error({ text: err.message });
				})
		} else {
			this._usersService.update(this.user.id, this.userRequest)
				.then((res) => {
					this._loader.hide();
					return this._alert.success({ text: 'El usuario ha sido actualizado exitosamente' })
						.then((res) => {
							if (res.value == true)
								this._router.navigate(['..'], { relativeTo: this._activatedRoute });
						});;
				}, (err) => {
					this._loader.hide();
					this._alert.error({ text: err.message });
				})
		}
		this._loader.hide();
	}

	castToModel() {
		this.userRequest = new UserRequest();
		this.userRequest.id = this.isNew ? this.user.id : 0;
		this.userRequest.nombre = this.user.nombre;
		this.userRequest.apellido_Materno = this.user.apellido_Materno;
		this.userRequest.apellido_Paterno = this.user.apellido_Paterno;
		this.userRequest.telefono = this.user.telefono;
		this.userRequest.email = this.user.correo;
		this.userRequest.rolId = this.user.rol.id;
		this.userRequest.carrierId = this.user.carrier.id;
		this.userRequest.rfc = this.user.rfc;
		this.userRequest.razon_social = this.user.razon_social;
		this.userRequest.tipo_persona = this.user.tipo_persona;
		this.userRequest.typeUser = this.user.typeUser;
		this.userRequest.direccion = this.user.direccion;
		this.userRequest.numero_Interno_AG_ID = this.user.numero_Interno_AG_ID;
		this.userRequest.perfilId = this.user.perfilId;
		if (this.user.rol.modulos.length > 0) {
			this.user.rol.modulos.forEach(mod => {
				this.userRequest.modulos.push({ id: mod.id, tipo_permiso: mod.permiso });
			})
		}



	}

	checkPermisos(idModulo: number, nPermiso: number): boolean {
		if (this.user.id) {
			if (this.user.rol.modulos.find(x => (x.id == idModulo || x.acceso_id == idModulo) && x.permiso == nPermiso) != null)
				return true;
			else
				return false;
		}
	}

	checkPermisoEspecial() {
		this.user.rol.modulos = []
		if (this.user.permisos_especiales) {
			this.user.rol.id = -1;
			this.user.rol.modulos = this.lstModAct
		}
	}

	changeRol(idmod) {

		if (!this.user) {
			this.user = new User();
			this.user.rol = new Roll();
		}


		if (idmod != -1) {
			this.user.permisos_especiales = false;
			this.user.rol.modulos = this.rolls.find(x => x.id == idmod).modulos;
		}
		else {
			this.user.permisos_especiales = true;
			this.user.rol.modulos = this.lstModAct;
		}
	}

	addModule(idModulo, nPermiso) {
		if (!this.user.rol.modulos) this.user.rol.modulos = [];
		if (this.isInRoles(idModulo)) {
			for (let i = 0; i < this.user.rol.modulos.length; i++) {
				if (this.user.rol.modulos[i].id == idModulo && this.user.rol.modulos[i].permiso == nPermiso) {
					this.user.rol.modulos.splice(i, 1);
					break;
			
				}
				if (this.user.rol.modulos[i].id == idModulo && this.user.rol.modulos[i].permiso != nPermiso) {
					this.user.rol.modulos[i].permiso = nPermiso;
					break;
				}
			}
		} else {
			this.user.rol.modulos.push({ id: idModulo, permiso: nPermiso });
		}
	}

	isInRoles(idModulo: number): boolean {
		let flag = false;
		if (!this.user.rol.modulos || this.user.rol.modulos.length < 1) return false;
		for (let i = 0; i < this.user.rol.modulos.length; i++) {
			if (this.user.rol.modulos[i].id === idModulo) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	getProfiles() {
		return this._usersService.getProfiles()
		.then((res) => {
			this.lsPerfiles = res;
		});
	}



}
