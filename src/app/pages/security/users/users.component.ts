import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import { AlertsService } from '../../../services/shared/alerts.service';
import { LoginService } from 'src/app/services/login/login.service';
import { map } from 'rxjs/operators';


declare let $: any;
@Component({
	selector: 'ag-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
	[x: string]: any;
	@ViewChild('table') private table: ElementRef;

	users: any[] = [];
	search: any;
	noReg = 10;
	page: number = 1;
	constructor(
		private _users: UsersService,
		private _alert: AlertsService,
		private _loginService: LoginService,
		
	) {
	}


	ngOnInit() {
		this.getUsers();
	}

	ngAfterViewInit() {
		$(this.table.nativeElement).collapsible();
	}

	getUsers(): void {
		this._users.get()
			.then(res => {
				this.users = res;
			    this.users=res.map(user=>({...user,nombre:user.nombre+" "+user.apellido_Paterno+" " +user.apellido_Materno}));
				setTimeout(() => $(this.table.nativeElement).collapsible('open', 0), 100);
			}, (err) => {
				setTimeout(() => $(this.table.nativeElement).collapsible('open', 0), 100);
			});
         
	}

	deleteUser(id: number) {
		this._alert.question({
			text: '¿Estás seguro que deseas eliminar este usuario?',
			showCancelButton: true
		}).then((result) => {
			console.log(result);
			if (result.value)
				return this._users.delete(id)
		}).then((data) => {
			return this._alert.success({ text: data.message })
		}, (err) => {
			this._alert.error({ text: err.message, title: 'Ocurrio un error' });
		}).then(() => {
			const index: number = this.users.findIndex(x => x.id === id);
			this.users.splice(index, 1);
		});
	}

	resetUsers(mail: string) {
		this._alert.question({
			text: '¿Estás seguro que deseas reestablecer la contraseña de este usuario?',
			showCancelButton: true
		}).then((result) => {
			if (result.value)
				return this._loginService.resetPassword(mail)
		}).then(() => {
			this._alert.success({ text: `Se ha enviado un mail con la contraseña temporal a: ' ${mail}` });
		}, (err) => {
			this._alert.error({ text: err.message });
		});;
	}

	toggleState(user) {


		this._alert.question({
			text: `¿Estás seguro que deseas ${user.activo ? 'bloquear' : 'desbloquear'} este usuario?`,
			showCancelButton: true
		}).then((result) => {
			if (result.value)
				return this._users.toggleActive(user.id);
		}).then((data) => {
			user.activo = !user.activo;
			this._alert.success({ text: data.message });
			this.getUsers()
			if (this.search.trim() != '')
				this.search = '';
		}, (err) => {
			this._alert.error({ text: err.message, title: 'Ocurrio un error' });
		});
	}

	trackByFn(index: number, item: any) {
		return item.id
	}

}
