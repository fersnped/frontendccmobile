import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertsService } from '../../../services/shared/alerts.service';
import { PasswordValidation } from 'src/app/validators/password.validation';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
	selector: 'ag-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
	form: FormGroup;
	formSubmitted: boolean = false;

	constructor(
		private _auth: AuthService,
		private _alert: AlertsService,
		private _fb: FormBuilder,
		private _router: Router,
		private _loginService: LoginService
	) {
	}

	ngOnInit() {
		this.buildForm();
	}

	buildForm() {
		this.form = this._fb.group({
			old: ['', Validators.required],
			passwords: this._fb.group({
				new: ['', Validators.required],
				confirm: ['', Validators.required]
			}, { validator: PasswordValidation.MatchPassword })
		});

		this.form.valueChanges
			.subscribe((value) => {
				this.formSubmitted = false;
			});
	}

	changePassword(event: Event) {
		event.preventDefault();

		this.formSubmitted = true;

		if (this.form.invalid) {
			this._alert.warning({ text: 'Revisa los campos antes de enviar' });
			return;
		}

		this._loginService.changePassword({
			user: this._auth.getUser().id,
			passwords: {
				old: <string>this.form.get('old').value,
				new: <string>this.form.get('passwords.new').value
			}
		}).then(res => {
			return this._alert.success({
				text: res.message
			});
		}, (err) => {
			this._alert.error({ text: err.message });
		}).then(() => {
			this._auth.destroySession();
			this._router.navigate(['/login']);
		});;
	}

}
