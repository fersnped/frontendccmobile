import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
// Components
import {ChangePasswordComponent} from './change-password/change-password.component';
import {UsersComponent} from './users/users.component';
import {RollsComponent} from './rolls/rolls.component';
// Routes
import {ROUTES_SECURITY} from './security.routing';
import {UsersDetailsComponent} from './users/users-details/users-details.component';
import {RollsDetailsComponent} from './rolls/rolls-details/rolls-details.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PipesModule} from '../../pipes/pipes.module';
import {ActivationComponent} from './activation/activation.component';
import {ActivacionNewComponent} from './activation/activacion-new/activacion-new.component';
import {ConfirmacionComponent} from './activation/confirmacion/confirmacion.component';
import {ActivacionDetalleComponent} from './activation/detalle/activacion-detalle.component';
import {ActivatekitComponent} from './activation/activatekit/activatekit.component';
import {MatButtonModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatTableModule} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from 'src/app/shared/shared.module';
import {RecargasModule} from './recargas/recargas.module';
import { NgxPaginationModule } from "ngx-pagination";




@NgModule({
  declarations: [
    ChangePasswordComponent,
    UsersComponent,
    RollsComponent,
    UsersDetailsComponent,
    RollsDetailsComponent,
    ActivationComponent,
    ActivacionNewComponent,
    ConfirmacionComponent,
    ActivacionDetalleComponent,
    ActivatekitComponent,
    
  ],
  imports: [
    ROUTES_SECURITY,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,    
    BrowserAnimationsModule,
    MatSelectModule,
    RecargasModule,
    SharedModule,
    NgxPaginationModule
  ],
  exports: []
})
export class SecurityModule {
}
