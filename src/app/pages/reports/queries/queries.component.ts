import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  ChangeDetectorRef
} from "@angular/core";
import { ReportsService } from "src/app/services/reports/reports.service";
import { MatSort, MatPaginator, MatTableDataSource } from "@angular/material";
import { IConsulta } from "src/app/models/consulta";
import { AlertsService } from "src/app/services/shared/alerts.service";
import { IProducts } from "src/app/models/request/products";
import * as moment from "moment";
declare let $: any;

@Component({
  selector: "ag-queries",
  templateUrl: "./queries.component.html",
  styleUrls: ["./queries.component.scss"]
})
export class QueriesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("products") private products: ElementRef;
  [x: string]: any;
  search: any;
  page: number = 1;
  noReg = 9;
  displayedColumns = [
    "noCelular",
    "imei",
    "iccid",
    "status",
    "statusTelcel",
    "error",
    "producto",
    "fechaActivacion"
  ];
  dataSource: MatTableDataSource<IConsulta>;
  isEmpty = true;
  loading: boolean = false;
  nNumCel: number;
  lstDetails: any[] = [];

  options = {
    format: "yyyy-mm-dd",
    maxDate: new Date(),
    autoClose: true,
    showClearBtn: true
  };

  fechaInicio: string = "";
  fechaFin: string = "";
  request: any = {
    producto: 1
  };

  lstProductos: IProducts[] = [];
  constructor(
    private cd: ChangeDetectorRef,
    private _reportsServices: ReportsService,
    private _alertsService: AlertsService
  ) {}

  ngOnInit() {
    this.fetchProducts()
      .then(data => {
        this.lstProductos = data;
        console.log(this.lstProductos);
        this.initComponents();
      })
      .catch(err => {
        this._alertsService.error({ text: err });
      });
  }

  async searchNumer() {
    this.loading = true;
    try {
      const res: any = await this._reportsServices.getQueries({
        ...this.request,
        fechaFin: this.fechaFin,
        fechaInicio: this.fechaInicio
      });
      this.isEmpty = res <= 0;
      this.lstDetails = res;
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      if (this.isEmpty)
        this._alertsService.warning({
          title: "No existen registros para este numero"
        });
    } catch (error) {
      this._alertsService.error({ text: error });
    } finally {
      this.loading = false;
    }
  }
  changeLast(e) {
    if (!e.target || !e.target.value) return;
    this.fechaFin = moment(new Date(e.target.value)).format("YYYY-MM-DD");
  }
  changeFirst(e) {
    if (!e.target || !e.target.value) return;
    this.fechaInicio = moment(new Date(e.target.value)).format("YYYY-MM-DD");
  }
  fetchProducts() {
    return this._reportsServices.getProducts().then((res: any) => {
      return res;
    });
  }

  initComponents() {
    setTimeout(() => {
      this.cd.detectChanges();
      $(this.products.nativeElement).formSelect();
      $("#fechaInicio").datepicker({
        ...this.options,
        onSelect: this.changeFirst
      });
      $("#fechaFin").datepicker({ ...this.options, onSelect: this.changeLast });
    }, 500);
  }

  async getExcel() {
    var result = await this._reportsServices.getQueriesExcel(this.request);
    window.open(result.url, "_blank");
  }

  // searchNumerTelcel() {
  //   if (this.nNumCel) {
  //     this._reportsServices.getQueriesTelcel(this.nNumCel)
  //       .then((res: any) => {
  //         this.isEmpty = res <= 0;
  //         this.lstDetails = res
  //         this.dataSource = new MatTableDataSource(res);
  //         this.dataSource.paginator = this.paginator;
  //         this.dataSource.sort = this.sort;
  //         if (this.isEmpty)
  //           this._alertsService.warning({ title: "No existen registros para este numero en telcel" })
  //       })
  //   }
  // }
}
