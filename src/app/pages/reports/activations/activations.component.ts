import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { IReportsActivations } from 'src/app/models/reportsActivations';
import { IClient } from 'src/app/models/client';
import { SalesService } from 'src/app/services/sales/sales.service';
import { AlertsService } from 'src/app/services/shared/alerts.service';
import { ReportsService } from 'src/app/services/reports/reports.service';

declare var $: any;
@Component({
  selector: 'ag-activations',
  templateUrl: './activations.component.html',
  styleUrls: ['./activations.component.scss']
})
export class ActivationsComponent implements OnInit {

  @ViewChild('lstActiva') private lstActiva: ElementRef
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ["fechaActivacion", "horaActivacion", "imei",
    "iccid", "numeroCelular", "plan",
    "tarifa", "activador"]
  dataSource: MatTableDataSource<IReportsActivations>;

  options = {
    format: 'yyyy-mm-dd',
    maxDate: new Date(),
    autoClose: true,
    showClearBtn: true
  };
  request = {
    usuarioId: '-1',
    fechaInicio: "",
    fechaFin: "",
    codigo: ""
  }

  isEmpty = true;

  lstActivadores: IClient[] = [];


  constructor(private cd: ChangeDetectorRef,
    private _salesService: SalesService,
    private _reportsService: ReportsService,
    private _alertsService: AlertsService) { }

  ngOnInit() {
    this.fetchActivadores()
      .then((data: any) => {
        this.lstActivadores = data;
        if (this.lstActivadores.length > 0)
          this.initComponents();
        else {
          this._alertsService.warning({ text: "No hay activadores", title: "Alerta" })
        }
      })
      .catch((err) => {
        console.log(err)
        this._alertsService.error({ text: err, title: "Error" })
      })
  }

  fetchActivadores() {
    return this._salesService.getAgNames(3)
      .then((data: any) => {
        return data;
      })
  }

  initComponents() {
    setTimeout(() => {
      this.cd.detectChanges()
      $(this.lstActiva.nativeElement).formSelect()
      $('#fechaInicio').datepicker(this.options);
      $('#fechaFin').datepicker(this.options);
    }, 1000);
  }

  consultaActivaciones() {
    this.request.fechaInicio = $('#fechaInicio').val();
    this.request.fechaFin = $('#fechaFin').val();

    this._reportsService.getActivaciones(this.request)
      .then((res: any) => {
        this.isEmpty = res <= 0;
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        if (this.isEmpty)
          this._alertsService.warning({ title: "No existen registros a mostrar" })
      })
  }

  async descargaExcel() {
    this.request.fechaInicio = $('#fechaInicio').val();
    this.request.fechaFin = $('#fechaFin').val();
    var result = await this._reportsService.getActivacionesExcel(this.request)
    window.open(result.url, '_blank');
  }



}
