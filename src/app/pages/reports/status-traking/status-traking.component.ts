import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { ReportsService } from 'src/app/services/reports/reports.service';
import { AlertsService } from 'src/app/services/shared/alerts.service';
import { IInventoryAvailable } from 'src/app/models/inventoryAvailable';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { isEmpty } from 'rxjs/operators';
import { SalesService } from 'src/app/services/sales/sales.service';
import { IClient } from 'src/app/models/client';
declare var $: any;
enum tipoVendedor {
  todos = 1,
  altaPendiente = 2,
  todosReporteVentas = 3
}

@Component({
  selector: 'ag-status-traking',
  templateUrl: './status-traking.component.html',
  styleUrls: ['./status-traking.component.scss']
})


export class StatusTrakingComponent implements OnInit,AfterContentChecked {

  //@ViewChild(MatPaginator) paginator: MatPaginator;
  res: any;
  @ViewChild(MatSort) sort: MatSort;
  [x: string]: any;
  search: any;
  page: number = 1;
  noReg = 9;
  displayedColumns = ['fechaOrdenVenta', 'folioVenta',
    // 'fechaActivacion', 'numeroCelular',
    'imei', 'vendedor'];
  dataSource: MatTableDataSource<IInventoryAvailable>;

  @ViewChild('sellers') private sellers: ElementRef

  optionsFact = ['No facturado', 'Facturado']

  options = {
    format: 'yyyy-mm-dd',
    maxDate: new Date(),
    autoClose: true,
    showClearBtn: true
  };
  request = {
    idVendedor: 0,
    fechaInicioActivacion: '',
    fechaFinActivacion: ''
  }

  isEmpty = true;
  bCheckFacturado: false;
  lstSubdistribuidores: {};

  constructor(
    private cd: ChangeDetectorRef,
    private _salesService: SalesService,
    private _reportsService: ReportsService,
    private _alertsService: AlertsService) { }
  ngAfterContentChecked(): void {
  }

  async ngOnInit() {

    await this.fetchVendedores()
    .then((res) => {
      this.lstSubdistribuidores = res.users
    })
    console.log(this.lstSubdistribuidores)
    this.initComponents();
  }


    initComponents() {
      setTimeout(() => {
        $(this.sellers.nativeElement).formSelect()
        this.cd.detectChanges()
        $('#fechaInicio').datepicker(this.options);
      $('#fechaFin').datepicker(this.options);
    }, 1000);

  }

  fetchVendedores() {
    return this._salesService.getAgNames(0)
      .then((data) => {
        return data
      })
  }

  searchInventory() {
    this.request.fechaInicioActivacion = $('#fechaInicio').val();
    this.request.fechaFinActivacion = $('#fechaFin').val();

    this._reportsService.getIventory(this.request)
      .then((res: any) => {
        this.isEmpty = res <= 0;
        this.res =res
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        if (this.isEmpty)
          this._alertsService.warning({ title: "No existen registros a mostrar" })
      })
  }

  async generarExcel() {
    var result = await this._reportsService.getExcelInventoryAvailable(this.request)
    window.open(result.url, '_blank');
  }

}
