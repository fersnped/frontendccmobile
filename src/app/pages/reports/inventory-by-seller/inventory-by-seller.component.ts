import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ReportsService } from 'src/app/services/reports/reports.service';
import { SalesService } from 'src/app/services/sales/sales.service';
import { IClient } from 'src/app/models/client';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { IInventorySeller } from 'src/app/models/inventorySeller';
import { AlertsService } from 'src/app/services/shared/alerts.service';
import { isEmpty } from 'rxjs/operators';
import { async } from 'rxjs/internal/scheduler/async';

declare let $: any;

@Component({
  selector: 'ag-inventory-by-seller',
  templateUrl: './inventory-by-seller.component.html',
  styleUrls: ['./inventory-by-seller.component.scss']
})
export class InventoryBySellerComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  res: any;
  [x: string]: any;
  search: any;
  page: number = 1;
  noReg = 9;
  displayedColumns = ['fechaOrdenVenta', 'folioVenta',
    // 'fechaActivacion', 'numeroCelular',
    'imei', 'vendedor'];
  dataSource: MatTableDataSource<IInventorySeller>;

  @ViewChild("sellers") private sellers: ElementRef

  lstSubdistribuidores: {};
  idSeller: string = ''

  lstDetails: IInventorySeller[] = []
  request = {
    idVendedor: 0,
    fechaInicioActivacion: '',
    fechaFinActivacion: ''
  }
  isEmpty = true;

  constructor(private _reportsService: ReportsService,
    private _salesService: SalesService,
    private cd: ChangeDetectorRef,
    private _alertsService: AlertsService) { }

  ngOnInit() {
    return this.fetchVendedores()
      .then((data) => {
        setTimeout(() => {
          this.initComponet();
        }, 1000);
      })

  }

  fetchVendedores() {
    return this._salesService.getAgNames(0)
      .then((data) => {
        this.lstSubdistribuidores = data.users
        return true
      })
  }

  searchInventory() {
    
      this._reportsService.getIventory(this.request)
        .then((res: any) => {
          // this.lstDetails = res;
          this.isEmpty = res <= 0;
          this.res= res;
          console.log(res)
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          if (this.isEmpty)
            this._alertsService.warning({ title: "No existen registros para este vendedor" })
        })
        .catch((err) => {
          this._alertsService.error({ text: err })
        })
    
  }

  initComponet() {
    this.cd.detectChanges()
    $(this.sellers.nativeElement).formSelect()
  }

  async generarExcel() {
    var result = await this._reportsService.getExcelInventoryAvailable(this.request)
    window.open(result.url, '_blank');
  }

}
