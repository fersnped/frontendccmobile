import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from 'src/app/services/guards/login.guard';
import { PagesComponent } from '../pages.component';
import { InventoryBySellerComponent } from './inventory-by-seller/inventory-by-seller.component';
import { QueriesComponent } from './queries/queries.component';
import { StatusTrakingComponent } from './status-traking/status-traking.component';
import { StatusComponent } from './status/status.component';
import { ActivationsComponent } from './activations/activations.component';
import { ActivoComponent } from './activo/activo.component';



// COMPONENTS

const ROUTES: Routes = [
    {
        path: 'reports',
        component: PagesComponent,
        //canActivate: [LoginGuard],
        children: [
            { path: 'inventory-seller', component: InventoryBySellerComponent },
            { path: 'queries', component: QueriesComponent },
            { path: 'reportSell', component: StatusTrakingComponent },
            { path: 'reportStatus', component: StatusComponent },
            { path: 'activations', component: ActivationsComponent },
            { path: 'activo', component: ActivoComponent  }

                    ]
    }
]
export const ROUTES_REPORTS = RouterModule.forChild(ROUTES);