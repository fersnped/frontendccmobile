import { Component, OnInit } from '@angular/core';
import { ReportsService } from 'src/app/services/reports/reports.service';
import { AlertsService } from 'src/app/services/shared/alerts.service';
import { IStatus } from '../../../models/response/status';
@Component({
  selector: 'ag-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  imei_iccid: string;
  isEmpty = true;

  lsStatus: IStatus;
  constructor(private _reportsService: ReportsService
              ,private _alertsService: AlertsService) { }

  ngOnInit() {
  }

  searchStatus(): void{
    this.isEmpty = false;
    if(this.imei_iccid) {
      if (this.imei_iccid.length < 15) {
        this._alertsService.warning({ title: "Lo longitud mínima permitida es de 15 caracteres" });
        return;
      }

      
      this._reportsService.getStatus({code : this.imei_iccid})
      .then(res => { 
        this.lsStatus = res;
        this.isEmpty = !(res != null);
        // if (this.isEmpty)
        //   this._alertsService.warning({ title: "No existen registros a mostrar" })
      });
    }
  }
}
