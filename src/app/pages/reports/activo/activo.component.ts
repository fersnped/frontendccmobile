import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import { ExcelService } from "src/app/services/excel.service";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'; const EXCEL_EXTENSION = '.xlsx';

declare let $: any;
@Component({
  selector: 'activo-users',
  templateUrl: './activo.component.html',
  styleUrls: ['./activo.component.scss']
})
export class ActivoComponent implements OnInit {

  @ViewChild('rol') private rol: ElementRef;
  [x: string]: any;
  users: any[] = [{}];
  page: number = 1;
  search: any;
  noReg = 10;

  excel: any = [{
  }]
  constructor(
    private _users: UsersService,
    private _excel: ExcelService
  ) {
  }
  ngOnInit() {
    this.getUsers()

  }

  getUsers(): void {
    this._users.get().then(res => {
      this.users = res;
      this.users = res.map(user => ({ ...user, modulos: user.rol.modulos, nombreCompleto: user.nombre + " " + user.apellido_Paterno + " " + user.apellido_Materno }));

    });


  }

  permisoModulo(modulos, id) {
    let moduloFind = modulos.find(modulo => modulo.acceso_id === id);

    if (moduloFind) {
      if (moduloFind.permiso == 1) {
        return 'L';
      } if (moduloFind.permiso == 2) {
        return 'L&E';
      }
    } else {
      return 'S/A';
    }
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json); const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] }; const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' }); this.saveAsExcelFile(excelBuffer, excelFileName);
  } private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE }); FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
  }

  exportAsXLSX(): void {
    this.exportAsExcelFile(this.generaExcel(), 'Reporte Usuario Activo');
  }

  generaExcel() {

    for (let i = 0; i < this.users.length; i++) {

      this.excel = [...this.excel, {
        NombreDeUsuario: this.users[i].nombre_Usuario,
        Nombre: this.users[i].nombre,
        ApellidoPaterno: this.users[i].apellido_Paterno,
        ApellidoMaterno: this.users[i].apellido_Materno,
        Numero_AG: this.users[i].numero_Interno_AG_ID,
        Ventas: this.permisoModulo(this.users[i].rol.modulos, 1),
        Activaciones_y_Recargas: this.permisoModulo(this.users[i].rol.modulos, 2),
        Banners: this.permisoModulo(this.users[i].rol.modulos, 3),
        Usuarios: this.permisoModulo(this.users[i].rol.modulos, 4),
        Roles_y_permisos: this.permisoModulo(this.users[i].rol.modulos, 5),
        Cambiar_Contraseña: this.permisoModulo(this.users[i].rol.modulos, 6),
        CallCenter: this.permisoModulo(this.users[i].rol.modulos, 7),
        Consultas_y_Recargas_Movil: this.permisoModulo(this.users[i].rol.modulos, 8),
        QR: this.permisoModulo(this.users[i].rol.modulos, 9),
        Reporte_de_Recarga: this.permisoModulo(this.users[i].rol.modulos, 10),
        Reporte_de_Ventas: this.permisoModulo(this.users[i].rol.modulos, 11),
        Inventario_por_Vendedor: this.permisoModulo(this.users[i].rol.modulos, 12),
        Consultas: this.permisoModulo(this.users[i].rol.modulos, 13),
        Reporte_Estatus_por_Serie: this.permisoModulo(this.users[i].rol.modulos, 14),
        Historial_Movil: this.permisoModulo(this.users[i].rol.modulos, 15),
        Activacion_y_Recarga_Movil: this.permisoModulo(this.users[i].rol.modulos, 16),
        Reporte_de_Activaciones: this.permisoModulo(this.users[i].rol.modulos, 18),
        Usuario_Activo: this.permisoModulo(this.users[i].rol.modulos, 19)

      }];

    }
    return this.excel
  }

}
