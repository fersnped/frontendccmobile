import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryBySellerComponent } from './inventory-by-seller/inventory-by-seller.component';
import { QueriesComponent } from './queries/queries.component';
import { ROUTES_REPORTS } from './reports.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { StatusTrakingComponent } from './status-traking/status-traking.component';
import { StatusComponent } from './status/status.component';
import { ActivationsComponent } from './activations/activations.component';
import { ActivoComponent } from './activo/activo.component';
import { NgxPaginationModule } from "ngx-pagination";



@NgModule({
  imports: [
    CommonModule,
    ROUTES_REPORTS,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    SharedModule,
    NgxPaginationModule
    
  ],
  declarations: [InventoryBySellerComponent, ActivoComponent , QueriesComponent, StatusTrakingComponent, StatusComponent, ActivationsComponent]
})
export class ReportsModule { }
