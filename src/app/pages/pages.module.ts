import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Components
import { HomeComponent } from "./home/home.component";
import { PagesComponent } from "./pages.component";

// import {SaleComponent} from './sale/sale.component'

import { OverlayModule } from "@angular/cdk/overlay";

// Routes
import { ROUTES_PAGES } from "./pages.routing";

// Modules
import { SharedModule } from "../shared/shared.module";
import { SecurityModule } from "./security/security.module";
import { QrModule } from "./qr/qr.module";
import { BannerModule } from "./banner/banner.module";
import { MzSelectModule } from 'ngx-materialize'
import { ReportsModule } from "./reports/reports.module";

import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
  declarations: [
    HomeComponent,
    PagesComponent
    // SaleComponent
  ],
  imports: [
    OverlayModule,
    CommonModule,
    MzSelectModule,
    // ROUTE
    ROUTES_PAGES,
    // MODULES
    SecurityModule,
    QrModule,
    BannerModule,
    SharedModule,
    ReportsModule,
    NgxPaginationModule
    
  ]
})
export class PagesModule { }
