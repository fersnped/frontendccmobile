import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from "@angular/core";
import { SalesService } from "src/app/services/sales/sales.service";
import { IClient, Client } from "src/app/models/client";
import { ActivatedRoute, Router } from "@angular/router";
import { ISales, Sales } from "src/app/models/sales";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/services/auth/auth.service";
import { ProductRequest } from "src/app/models/request/productRequest";
import { Imei } from "src/app/models/response/imei";
import Swal, { SweetAlertType } from "sweetalert2";
import { MzModalComponent } from "ngx-materialize";
import { CarriersService } from "src/app/services/carriers/carriers.service";
import { ICarrier } from "src/app/models/carrier";
import { AlertsService } from "src/app/services/shared/alerts.service";
import { IInventory } from "src/app/models/inventory";
import { IUsuariosVendedores } from "src/app/models/usuariosVendedores";
import { Util } from 'src/app/common/util';

declare let $: any;
class ImageSnippet {
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: "sale",
  templateUrl: "./newSales.component.html",
  styleUrls: ["./newSales.component.scss"]
})

export class NewSaleComponent implements OnInit {
  @ViewChild("cmbSubs") private cmbSubs: ElementRef;
  @ViewChild("selAgName") private selAgName: ElementRef;
  @ViewChild("bottomCrearSubdistribuidor") modal: MzModalComponent;
  @ViewChild("carrier") private carrier: ElementRef;
  @ViewChild("typeUser") private typeUser: ElementRef;
  @ViewChild("imei") private imei: ElementRef;
  form: FormGroup;
  isNew: Boolean;
  _item: ISales;
  submitted: boolean;
  validSubmitted: boolean;
  name: string;
  user: any;
  address: string;
  productsRequest: ProductRequest;
  validateResponse: Imei;
  imeiAsString: string;
  subRequest: any;
  lstVendedores: IUsuariosVendedores[] = [];
  sNombreVendedor: string = "";
  sNombreSubdistribuidor: string = "";
  lstSubdistribuidores: IClient[] = [];
  //agNameList: any[] = [];
  selectLabel = "Label";
  selectPlaceholder = "Placeholder";
  date: any;
  saleID: string;
  subId: string;
  carriers: ICarrier[] = [];
  lstImei: IInventory[] = [];
  selectedFile: ImageSnippet = null;
  selectedFileName: string;
  constructor(
    private _util: Util,
    private _salesService: SalesService,
    private _activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _router: Router,
    private _carriersService: CarriersService,
    private _alert: AlertsService,
    private cd: ChangeDetectorRef
  ) {
    this.selectedFileName = 'Logotipo'
    this.subId = "-1";
    this.date = new Date();
    this._item = new Sales();
    this.submitted = false;
    this.validSubmitted = false;
    this.name = "";
    this.address = "";
    this.imeiAsString = "";
    this.productsRequest = new ProductRequest();
    this.subRequest = {
      nombre: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      telefono: "",
      correo: "",
      carriedId: 0,
      numero_Interno_AG_ID: null,
      rfc: "",
      tipoPersonId: 1,
      razonSocial: "",
      direccion: "",
      typeUser: 0
    };
  }
  ngOnInit() {
    this.user = this._authService.getUser();
    this._activatedRoute.paramMap.subscribe(params => {
      this.saleID = params.get("id");
      if (this.saleID == "0") {
        this.isNew = true;
        this.saleID = "#####";
      }
    });
    if (this.isNew) {
      this.fetchCarriers()
        .then(carriers => {
          this.carriers = carriers;
          return this.fetchNumInt(0);
        })/*
        .then(agNum => {
          this.agNameList = agNum;
        })*/
        .catch(err => {
          this._alert.error({ title: "¡Ups!", text: err });
        });
    }
    this.fetchVendedores();
    this.fetchDis();
    this.fetchSale();
    this.buildModalForm();
    setTimeout(() => {
      this.initComponets();
    }, 500);
  }
  selectLogo(imageInput: any){
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {

      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.selectedFileName= this.selectedFile.file.name
      
    });

    reader.readAsDataURL(file);

  }
  fetchDis() {
    this._salesService.getSubdistribuidores().then(res => {
      this.lstSubdistribuidores = res;
      setTimeout(() => {
        $("#nameSubdistribuidor").blur();
      }, 200);
    });
  }
  fetchSale(): void {
    this._item = new Sales();
    this._item.subdistribuidor = new Client();
    this._item.subdistribuidor.id = -1;

    if (!this.isNew) {
      this._salesService.getById(parseInt(this.saleID)).then(sale => {
        this.sNombreVendedor = this.lstVendedores.find(
          x => x.usuarioId == sale.usuarioId
        ).nombre;
        this.productsRequest.subdistribuidorId =
          sale.subdistribuidor.usuario_id;
        if (sale.subdistribuidor.usuario_id == 0) this.subId = "-2";
        this._item.subdistribuidor.id = sale.subdistribuidor.usuario_id;
        this.subId = this._item.subdistribuidor.id.toString();
        this.address = sale.subdistribuidor.direccion;
        this.productsRequest.productos = sale.detalle_venta;
        let dateAsSplit = sale.fecha_creacion.split("/");
        if (dateAsSplit.length == 3) {
          this.date = new Date(
            dateAsSplit[2],
            parseInt(dateAsSplit[1]) - 1,
            dateAsSplit[0]
          );
        }
        this.initComponets();
      });
    }
  }
  fetchNumInt(id: number) {
    return this._salesService.getAgNames(id, 2).then(res => {
      return res;
    });
  }
  fetchCarriers() {
    return this._carriersService.get().then(carrier => {
      return carrier;
    });
  }
  fetchImei() {
    var nId = this.lstVendedores.find(x => x.nombre == this.sNombreVendedor)
      .usuarioId;
    this.imeiAsString = "";
    if (nId != null && nId != 0 && this.isNew) {
      this.productsRequest.ejecutivoId = nId;
      this._salesService.getImei(nId).then((res: any) => {
        this.lstImei = res;
        
        setTimeout(() => {
          $("#nameVendedor").blur();
        }, 200);
      });
    }
  }
  fetchVendedores() {
    this._salesService.getUsuariosVendedores().then((res: any) => {
      this.lstVendedores = res;
      this.sNombreVendedor = res[0].nombre;
      this.fetchImei();
      this.cd.detectChanges();
    });
  }
  changeTypeUser() {
    //this.agNameList = [];
    this.fetchNumInt(parseInt(this.subRequest.typeUser)).then(agNum => {
      //this.agNameList = agNum.filter(x => x.usuarioId != "all");
      //this.subRequest.numero_Interno_AG_ID = this.agNameList[0].usuarioId;
      this.initComponets();
    });
  }
  setAddress(): void {
    // let subIdAsInt = parseInt(this.subId);
    let sub = this.lstSubdistribuidores.find(
      x => x.nombre == this.sNombreSubdistribuidor
    );
    this.productsRequest.subdistribuidorId = sub ? sub.usuario_id : -1;
    this.address = sub ? sub.direccion : "";
    // this.productsRequest.subdistribuidorId = subIdAsInt;
  }
  initComponets() {
    this.cd.detectChanges();
    // $(this.cmbSubs.nativeElement).formSelect();
    //$(this.selAgName.nativeElement).formSelect();
    $(this.carrier.nativeElement).formSelect();
    $(this.typeUser.nativeElement).formSelect();
    // $(this.imei.nativeElement).formSelect();
  }
  validateImei() {
    this.validSubmitted = true;
    let indexByCode = this.productsRequest.productos.find(
      x => x.imei_iccid == this.imeiAsString
    );
    if (!indexByCode) {
      this._salesService
        .validateImei(this.imeiAsString, this.productsRequest.ejecutivoId)
        .then(res => {
          this.productsRequest.productos.push({
            imei_iccid: res.codigo,
            descripcion: res.descripcion_producto
          });
        });
    } else {
      this._alert.info({
        title: "¡Alerta!",
        text: "No puedes agregar el mismo producto, favor de revisar!"
      });
    }
    this.imeiAsString = "";
    this.validSubmitted = false;
  }
  deleteProduct(index: number): void {
    this.productsRequest.productos.splice(index, 1);
  }
  submit() {
    this.submitted = true;
    if (this.productsRequest.subdistribuidorId == -1) {
      this.showAlert(
        "¡Ups!",
        "Se debe seleccionar un subdistribuidor",
        "warning"
      );
    } else if (this.productsRequest.productos.length == 0) {
      this.showAlert(
        "¡Ups!",
        "Se debe de agregar al menos un producto.",
        "warning"
      );
    } else {
      this._salesService.create(this.productsRequest).then(res => {
        this.showAlert(
          "Venta generada exitosamente.",
          "Folio: " + res.folio,
          "success"
        );
        this._router.navigate(["/sale"]);
      });
    }
    this.submitted = false;
  }
  buildModalForm() {
    this.form = this._fb.group({
      name: [this.subRequest.nombre, [Validators.required]],
      lastName: [this.subRequest.apellidoPaterno, [Validators.required]],
      secondLastName: [this.subRequest.apellidoMaterno, [Validators.required]],
      number: [this.subRequest.telefono, [Validators.required]],
      email: [this.subRequest.correo, [Validators.required]],
      agNumber: [this.subRequest.numero_Interno_AG_ID,[Validators.required]],
      rfc: [this.subRequest.rfc, [Validators.required]],
      social: [this.subRequest.razonSocial],
      address: [this.subRequest.direccion, [Validators.required]],
      typeUser: [this.subRequest.typeUser],
      carriedId: [this.subRequest.carriedId, [Validators.required]]
    });
  }
  setGenericClient() {
    this.address = "Dirección Genérica";
    this.subId = "-2";
  }
  submitSubdis() {
    if (this.form.valid) {
      //console.log(this.selectedFile.file)
      
      
      if (
        this.subRequest.tipoPersonId == 2 &&
        this.subRequest.razonSocial.trim() == ""
      ) {
        this.showAlert(
          "!Advertencia!",
          "La razon social no puede ir vacia",
          "warning"
        );
        return;
      }
      //Aqui va la validación de correo 
      /*if(!this.isFileImage()){
        this.showAlert(
          "!Advertencia!",
          "El archivo seleccionado no es una imagen .png, .jpg o .jpeg",
          "warning"
        );
        return;
      }*/


      this._salesService.createSubdis(this.subRequest).then(res => {
        //console.log(res,"aqui bro");
        Swal.fire({
          title: "",
          text: "El subdistribuidor se ha creado con éxito",
          type: "success",
          showConfirmButton: false
        });
        setTimeout(() => {
          
        }, 2500);
        /*this._salesService.postLogo(this.selectedFile.file,res).then(res => {
          
        });*/
        
        
        this.form.reset();
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      });
    } else {
      console.log(this.form);
      this.showAlert("!Advertencia!", "Faltan algunos campos", "warning");
    }
  }

  isFileImage() {
    const acceptedImageTypes = ['image/jpg', 'image/jpeg', 'image/png'];
    if(this.selectedFile == null)
      return false;

    return this.selectedFile.file && acceptedImageTypes.includes(this.selectedFile.file['type'])
  }
  
  showAlert(title: string, message: string, type: SweetAlertType): void {
    Swal.fire(title, message, type);
  }
  keyupFilterSub() {
    this.sNombreSubdistribuidor = "";
  }
  keyupFilter() {
    this.sNombreVendedor = "";
  }
  changeImei() {
    this.imeiAsString = "";
  }
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: 0.5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: "100%", // Starting top style attribute
    endingTop: "10%", // Ending top style attribute

    ready: (modal, trigger) => {
      // Callback for Modal open. Modal and trigger parameters available.
      alert("Ready");
    },
    complete: () => {
      alert("Closed");
    } // Callback for Modal close
  };
}
