import { Component, OnInit, ViewChild } from '@angular/core';
import { SalesService } from 'src/app/services/sales/sales.service';
import { ISales } from 'src/app/models/sales';
import { Router } from '@angular/router';

declare let $:any;
@Component({
	selector: 'sale',
	templateUrl: './sale.component.html',
	styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {
	displayedColumns = ['NumeroDeOrden', 'NumeroDeITEMS', 'Cliente', 'Fecha'];
	fechaInicioBusqueda: string;
	fechaFinBusqueda: string;
	lstSales: ISales[] = [];
	isLoading: boolean;
	dataSource: any = [];
	[x: string]: any;
  	search: any;
  	page: number = 1;
  	noReg = 9;
	// @ViewChild('sellers') private sellers: ElementRef
	options = {
		format: 'yyyy-mm-dd',
		maxDate: new Date(),
		autoClose: true,
		showClearBtn: true
	  };
	constructor(private _Sales: SalesService, private _router: Router) {
		
			let d = new Date();
			let curr_date = d.getDate();
			let curr_month = d.getMonth() + 1;
			let curr_year = d.getFullYear();
			this.fechaInicioBusqueda = '';//curr_year + "-" + (curr_month < 10? ("0" + curr_month) : curr_month) + "-" + (curr_date < 10? ("0" + curr_date) : curr_date);
			this.fechaFinBusqueda = '';//curr_year + "-" + (curr_month < 10? ("0" + curr_month) : curr_month) + "-" + (curr_date < 10? ("0" + curr_date) : curr_date);
	}

	ngOnInit() {
		this.isLoading = true;
		 this.getSale();
		 $('.datepicker').datepicker(this.options);
	}

	getSale() {
		if(this.getIsValid()){
			let fechaInicio: string = $('#fechaInicio').val().toString();
			let fechaFin: string = $('#fechaFin').val().toString();

			this._Sales.getSale(fechaInicio, fechaFin)
				.then((res: any) => {
					this.lstSales = res
			});
		}
	}

	goToSale(id) {
		this._router.navigate(['/sale', id])
	}

	getIsValid(): boolean {
		if($('#fechaInicio').val() === "") {
			return false;
	  }

	  if($('#fechaFin').val() === ""){
		return false;
	  }
		  
		return true;
	}
}
