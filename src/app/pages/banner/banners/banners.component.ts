import { Component, OnInit } from '@angular/core';
import { BannersService } from 'src/app/services/banners/banners.service';
import { Banners } from 'src/app/models/banners';
import { AlertsService } from 'src/app/services/shared/alerts.service';

@Component({
  selector: 'ag-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {
  [x: string]: any;
  search: any;
  page: number = 1;
  noReg = 9;
  banners: Banners[] = [] /*[{
    id: 1,
    nombre: 'banner1',
    descripcion: 'aqui va el banner 1',
    Fecha_Inicio_Publicacion: '01/01/01',
    image: 'sevsevfs'
},
{
  id: 2,
  nombre: 'banner2',
  descripcion: 'aqui va el banner 2',
  Fecha_Inicio_Publicacion: '01/01/01',
  image: 'lnjkblseu'
}]*/
    
  constructor(
    private _alert: AlertsService,
    private _bannerService: BannersService
  ) { }

  ngOnInit() {
    this.getBanners();
  }

  getBanners() {

    this._bannerService.getBanners()
      .then((res: any) => {
        this.banners = res;
      })
      console.log(this.banners);
      
  }

  deleteBanner(nIdBanner: number) {
    this._alert.warning({ title: "Eliminar Banner", text: "¿Esta seguro que desea eliminar el banner?", showCancelButton: true })
      .then((data: any) => {
        if (data.value) {
          this._bannerService.deleteBanner(nIdBanner)
            .then((data: any) => {

              this._alert.success({ text: "Exito al eliminar" }).then(() => { this.getBanners(); })
            })
        }
      })



  }


}
