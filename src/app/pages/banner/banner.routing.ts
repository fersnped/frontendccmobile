import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from '../pages.component';
import { LoginGuard } from 'src/app/services/guards/login.guard';

import { BannerUploadComponent } from './banner-upload/banner-upload.component';
import { BannersComponent } from './banners/banners.component';
const ROUTES: Routes = [
    {
        path: 'banners',
        component: PagesComponent,
        canActivate: [LoginGuard],
        children: [
            { path: '', component: BannersComponent },
            { path: 'uploadBanner', component: BannerUploadComponent },
            { path: ':id', component: BannerUploadComponent }
        ]
    }
]

export const ROUTES_BANNER = RouterModule.forChild(ROUTES);