import { Component, OnInit } from '@angular/core';
import { FileUpload } from 'src/app/models/fileUpload';
import { AlertsService } from 'src/app/services/shared/alerts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Util } from 'src/app/common/util';
import { AuthService } from 'src/app/services/auth/auth.service';
import { slide } from 'src/app/animations/slide';
import { Banners, IBanners } from 'src/app/models/banners';
import { BannersService } from 'src/app/services/banners/banners.service';



declare let $: any;
@Component({
  selector: 'ag-banner-upload',
  templateUrl: './banner-upload.component.html',
  styleUrls: ['./banner-upload.component.scss'],
  animations: [slide]
})
export class BannerUploadComponent implements OnInit {


  bIsLoading: boolean = true;
  isLoading: boolean = false;
  submitted: boolean = false;
  files: FileUpload[] = [];
  lstTypes: string[] = ["png"]
  sTypesAccept: string = ".png";
  isActions: number = 0;
  banners: Banners[] = [];
  nIdBanner: number;
  banner: IBanners;
  constructor(
    private _util: Util,
    private router: Router,
    private _alert: AlertsService,
    private _activatedRoute: ActivatedRoute,
    private _bannerService: BannersService,
    private _router: Router
  ) { }


  ngOnInit() {
    this.bIsLoading = false;
    this._activatedRoute.paramMap.subscribe((params: any) => {
      this.nIdBanner = parseInt(params.get('id'));
      if (this.nIdBanner) {
        this._bannerService.getBanner(this.nIdBanner)
          .then((data: any) => {
            this.banner = Object.assign({}, data); data;
            this.cargarImagen();
          })
      }
    })

  }
  getBanners() {

    this._bannerService.getBanners()
      .then((res: any) => {
        this.banners = res;
      })
      console.log(this.banners);
      this._router.navigate(['/banners']);
  }
  deleteBanner(nIdBanner: number) {
    this._alert.warning({ title: "Eliminar Banner", text: "¿Esta seguro que desea eliminar el banner?", showCancelButton: true })
      .then((data: any) => {
        if (data.value) {
          this._bannerService.deleteBanner(nIdBanner)
            .then((data: any) => {

              this._alert.success({ text: "Exito al eliminar" }).then(() => { 
                this.getBanners();
                return;
               })
            })
        }
      })



  }

  displayFiles(event: any): void {
    this.isActions = 3;
    this._util.displayFile(event)
      .then((data: any) => {
        if (this.files.length > 0)
          this.files = [];
        this.files.push(data);
        $("#file-upload").val('');
        this.isActions = 4;

        this.validarDimensiones();
      }, (err) => {
        if (err.code == "NO_FILE") {
          if (this.files.length > 0)
            this.files = [];
        }
        this._alert.error({ text: err.message });
        this.isActions = 0;
      });
  }

  dropFiles(event: any): void {
    this.isActions = 2;
    this._util.dropHandler(event, this.lstTypes)
      .then((data) => {
        let files = [];
        for (let item of data) {
          files.push(item.data);
        }
        this.displayFiles({ target: { files: files } });
      }, (err) => {
        this._alert.error({ text: err.message });
        this.isActions = 0;
      });
  }

  dragoverFile(event: any): void {
    this.isActions = 1;
    this._util.dragoverHandler(event);
  }

  dragEnd(event: any) {
    this.isActions = 0;
    this._util.dragendHandler(event);
  }

  removeFiles(index?: number): void {
    this.isActions = 0;
    this.files = []
  }

  enviarImagen() {
    console.log(this.files[0].file);
    this.bIsLoading = true;
    let formData = new FormData();
    formData.append("image", this.files[0].file)
    
    this._bannerService.postBanner(formData)
      .then((data: any) => {
        this._alert.success({ title: "Exito al cargar", text: "Se ha cargado el banner correctamente" })
          .then((res: any) => {
            if (res.value) {
              this.bIsLoading = false;
              this.router.navigate([""])

            }
          })
      })
      .catch((err: any) => {
        this.bIsLoading = false;
        this._alert.error({ title: "Error al cargar el archivo", text: err.message })
      });

  }

  cancelarImagen() {
    this.files = [];
    this.isActions = 0;
    this.router.navigate([""])
  }


  validarDimensiones() {

    let bFlagDimension = false;
    let image = new Image();
    image.src = this.files[0].base64;
    image.onload = (loadEvent: any) => {
      let maxHeight = 1080;
      let maxWidth = 1920;
      var height = loadEvent.path[0].height;
      var width = loadEvent.path[0].width;
      if (height > maxHeight && width > maxWidth) {
        this._alert.error({ text: "Las dimensiones de la imagen son superiores a 1920 x 1080" })
        bFlagDimension = true;
        this.files = [];
        this.isActions = 0;

        return;
      }
      else if (height > maxHeight) {
        this._alert.error({ text: "El alto de la imagen es superior a 1080" })
        bFlagDimension = true;
        this.files = [];
        this.isActions = 0;

        return;
      }
      else if (width > maxWidth) {
        this._alert.error({ text: "El ancho de la imagen es superior a 1920" })
        bFlagDimension = true;
        this.files = [];
        this.isActions = 0;

        return;
      }
    };

  }

  updateBanner() {
    this.bIsLoading = true;
    let formData = new FormData();
    formData.append("image", this.files[0].file)
    this._bannerService.updateBanner(this.nIdBanner, formData)
      .then((data: any) => {
        this._alert.success({ title: "Exito al cargar", text: "Se ha cargado el banner correctamente" })
          .then((res: any) => {
            if (res.value) {
              this.bIsLoading = false;
              this.router.navigate([""])
            }
          })
      })
      .catch((err: any) => {
        this.bIsLoading = false;
        this._alert.error({ title: "Error al cargar el archivo", text: err.message })
      });
  }

  cargarImagen() {
    if (this.nIdBanner) {

      this.files[0] = {
        name: this.banner.nombre,
        base64: this.banner.image,
        size: 0
      }
      this.isActions = 4;
    }




  }


}
