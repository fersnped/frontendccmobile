import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannersComponent } from './banners/banners.component';
import { BannerUploadComponent } from './banner-upload/banner-upload.component';
import { ROUTES_BANNER } from './banner.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
  imports: [
    CommonModule,
    ROUTES_BANNER,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    SharedModule,
    NgxPaginationModule
  ],
  declarations: [BannersComponent, BannerUploadComponent]
})
export class BannerModule { }
