import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertsService } from '../../services/shared/alerts.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginService } from 'src/app/services/login/login.service';
//import { userInfo } from 'os';

@Component({
	selector: 'ag-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	form: FormGroup;

	constructor(
		private _auth: AuthService,
		private _loginService: LoginService,
		private _fb: FormBuilder,
		private _alert: AlertsService,
		private _router: Router
	) { }

	ngOnInit() {
		if (this._auth.isLogged()) {
			if (!this._auth.getUser().primer_Login) {
				this._router.navigate(['/home']);
				return;
			}
		}
		this._router.navigate(['/home']);//
		this.buildForm();
	}

	buildForm() {
		this.form = this._fb.group({
			user: ['', Validators.required],
			password: ['', Validators.required]
		});
	}

	login(event: Event): void {
		event.preventDefault();

		if (this.form.invalid) {
			this._alert.warning({ text: 'Revisa los campos antes de enviar' });
			return;
		}

		this._loginService.login(this.form.value)
			.then((res) => {
				console.log(res)
				console.log(res.user.primer_Login);
				this._auth.setSession({ user: res.user, token: res.token });
				if (res.user.primer_Login)
					this._router.navigate(['/login/change-password']);
				else
					//this._auth.setSession({ user: res.user, token: res.token })
					this._router.navigate(['/home']);
			});
	}

}
