import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordValidation } from '../../validators/password.validation';
import { AlertsService } from '../../services/shared/alerts.service';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
	selector: 'ag-first-login',
	templateUrl: './first-login.component.html',
	styleUrls: ['./first-login.component.css']
})
export class FirstLoginComponent implements OnInit {
	form: FormGroup;
	formSubmitted: boolean = false

	constructor(
		private _fb: FormBuilder,
		private _alert: AlertsService,
		private _router: Router,
		private _loginService: LoginService,
		private _auth: AuthService
	) { }

	ngOnInit() {
		this.buildForm();
	}

	buildForm() {
		this.form = this._fb.group({
			old: ['', Validators.required],
			passwords: this._fb.group({
				new: ['', Validators.required],
				confirm: ['', Validators.required]
			}, { validator: PasswordValidation.MatchPassword })
		});

		this.form.valueChanges
			.subscribe((value) => {
				this.formSubmitted = false;
			});
	}

	changePassword(event: Event) {
		event.preventDefault();
		this.formSubmitted = true;
		console.log("evsrv")
		if (this.form.invalid) {
			this._alert.warning({ text: 'Revisa los campos antes de enviar' });
			return;
		}

		this._loginService.changePassword({
			user: this._auth.getUser().id,
			passwords: {
				old: this.form.get('old').value,
				new: this.form.get('passwords.confirm').value
			}
		}).then(res => {
			return this._alert.success({ text: res.message });
		}, (err) => {
			this._alert.error({ text: err.message });
		}).then(result => {
			this._auth.destroySession();
			this._router.navigate(['/login']);
		});
	}
}
