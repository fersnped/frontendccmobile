import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AlertsService } from '../../services/shared/alerts.service';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
	selector: 'ag-recovery-password',
	templateUrl: './recovery-password.component.html',
	styleUrls: ['./recovery-password.component.scss']
})
export class RecoveryPasswordComponent implements OnInit {
	mailControl: FormControl;
	formSubmitted: boolean = false;
	isLoading: boolean = false;

	constructor(
		private _loginService: LoginService,
		private _alert: AlertsService,
		private _router: Router
	) { }

	ngOnInit() {
		this.mailControl = new FormControl('', [Validators.email, Validators.required]);
	}

	sendMail() {
		this.formSubmitted = true;

		if (this.mailControl.invalid) {
			this._alert.warning({ text: 'Revisa bien el campo antes de enviar' });
			return;
		}

		this.isLoading = true;

		this._loginService.resetPassword(<string>this.mailControl.value)
			.then(() => {
				this.isLoading = false;
				return this._alert.success({
					text: 'Se ha enviado un correo electrónico con una contraseña temporal'
				});
			}, (err) => {
				this.isLoading = false;
				this._alert.error({ text: err.message });
			}).then((result) => {
				this._router.navigate(['/login']);
			});;
	}

}
