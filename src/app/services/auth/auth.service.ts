import { Injectable } from '@angular/core';
import { CURRENT_USER } from 'src/app/config/config';
import { IUser, User } from 'src/app/models/user';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor() { }

	isLogged(): boolean {
		const token = this.getToken();
		return token !== undefined && token !== null;
	}

	setSession(data: { user: IUser, token: string }) {
		localStorage.setItem(CURRENT_USER, JSON.stringify(data));
	}

	destroySession(): void {
		localStorage.removeItem(CURRENT_USER);
		localStorage.removeItem("nMenuOpen");
	}

	getToken(): string {
		let token = undefined;

		if (localStorage.getItem(CURRENT_USER)) {
			token = JSON.parse(localStorage.getItem(CURRENT_USER)).token;
		}

		return token;
	}

	getUser(): IUser {
		let user: IUser = new User();

		if (localStorage.getItem(CURRENT_USER)) {
			user = JSON.parse(localStorage.getItem(CURRENT_USER)).user;
		}

		return user;
	}
}
