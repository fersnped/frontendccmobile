import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LoginGuard } from "./guards/login.guard";

import { HttpModule } from "@angular/http";
import { AlertsService } from "./shared/alerts.service";
import { UsersService } from "./users/users.service";
import { RollsService } from "./rolls/rolls.service";
import { CarriersService } from "./carriers/carriers.service";
import { LoaderService } from "./shared/loader.service";
import { AuthService } from "./auth/auth.service";
import { ModulesService } from "./modules/modules.service";
import { LoginService } from "src/app/services/login/login.service";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { CustomHttpInterceptorService } from "src/app/services/http/custom-http-interceptor.service";
import { ActivationService } from "./activation/activation.service";
import { BannersService } from "src/app/services/banners/banners.service";
import { QrService } from "src/app/services/qr/qr.service";
import { SalesService } from "../services/sales/sales.service";
import { DistributorService } from "../services/distributor/distributor.service";
import { RecargasService } from "./recargas/recargas.service";
import { ReportsService } from "./reports/reports.service";
import { SidebarService } from "./sidebar/sidebar.service";
import { ExcelService } from "./excel.service";

@NgModule({
  imports: [CommonModule, HttpModule],
  providers: [
    AuthService,
    AlertsService,
    LoginService,
    ModulesService,
    UsersService,
    RollsService,
    LoginGuard,
    CarriersService,
    LoaderService,
    ActivationService,
    QrService,
    BannersService,
    SalesService,
    DistributorService,
    RecargasService,
    ReportsService,
    SidebarService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptorService,
      multi: true
    },
    ExcelService
  ],
  declarations: []
})
export class ServicesModule {}
