import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { IModulesResponse } from 'src/app/models/response/modules';

@Injectable()
export class ModulesService {
	private url = '/modulos';

	constructor(
		private _http: CustomHttpService
	) { }

	get(): Promise<any[]> {
		return this._http.get<IModulesResponse[]>(this.url)
			.then((res: any) => {
				return res.data.modulos;
			});
	}
}
