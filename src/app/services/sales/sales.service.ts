import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { ISalesResponse, ISaleResponse } from 'src/app/models/response/sales';
import { ISubdistribuidorResponse } from 'src/app/models/response/client';
import { IImei } from 'src/app/models/response/imei';
import { ISales } from 'src/app/models/sales';
import { IInventoryResponse } from 'src/app/models/response/inventory';
import { IUsuariosVendedoresResponse } from 'src/app/models/response/usuariosVendedores';

@Injectable()
export class SalesService {
	private urlGet: string = '/getVenta';
	private urlGetSaleById: string = '/getVentaById/';
	private urlGetSubdistribuidor: string = '/getSubdistribuidor';
	private urlValidateIMEI: string = '/validarICCIDIMEI/';
	private urlPostSale: string = '/postVenta';
	private urlPostSubdis: string = '/postSubdistribuidor';
	private urlGetAGName: string = '/getAG';
	private urlGetVendedores: string = '/usuariosVendedores';
	private urlGetUsuariosAG: string = '/usuariosAG';
	private urlGetInventario = "/getInventario";
	private urlGetUsuariosVendedores = '/getUsuariosVendedores';
	private urlGetSale = '/getVenta';
	private urlGetActivadores = "/activadores";
	constructor(
		private _http: CustomHttpService
	) { }
	postLogo(image: File,idSubdis: number ): Promise<any> {
		 
		const formData = new FormData();
		formData.append('image', image);
		formData.append('userID', idSubdis.toString());
	
		return this._http.post<any, FormData>("/postLogo", formData, { headers: { 'Content-Type': 'multipart/form-data' } })
		.then((res: any) => {
			return res.data
		})
	}
	get(): Promise<ISalesResponse> {
		return this._http.get<ISalesResponse>(this.urlGet)
			.then((res: any) => {
				return res.data.ventas;
			});
	}

	getSale(fechaInicio: string, fechaFin: string): Promise<ISalesResponse> {
		return this._http.get<ISalesResponse>(this.urlGetSale + "/" + fechaInicio + "/" + fechaFin)
			.then((res: any) => {
				return res.data.ventas;
			});
	}

	getById(id: number) {
		return this._http.get<any>(this.urlGetSaleById + id)
			.then((res) => {
				return res.data;
			})
	}

	getSubdistribuidores() {
		return this._http.get<ISubdistribuidorResponse>(this.urlGetSubdistribuidor)
			.then((res: any) => {
				return res.data.subdistribuidor;
			})
	}

	validateImei(imei: string, nIdUsuario: number) {
		return this._http.post<IImei, any>(this.urlValidateIMEI, { codigo: imei, usuarioId: nIdUsuario })
			.then((res: any) => {
				return res.data;
			})
	}

	

	create(requestBody) {
		return this._http.post(this.urlPostSale, requestBody)
			.then((res: any) => {
				return res.data;
			});
	}

	getAgNames(id: number, nTipoVendedor?: number) {

		let finalUrl = "";
		switch (id) {
			case 0:
				finalUrl = this.urlGetUsuariosAG;
				break;
			case 1:
				/// 1 -> Reportes, trae el valor de todos
				/// 2 -> Agregar, trae el valor de alta pendiente
				finalUrl = this.urlGetVendedores + '/' + nTipoVendedor;
				break;
			case 2:
				finalUrl = this.urlGetAGName;
				break;
			case 3:
				finalUrl = this.urlGetActivadores;
				break;
			default:
				finalUrl = this.urlGetUsuariosAG;
				break;
		}
		return this._http.get(finalUrl)
			.then((res: any) => {
				console.log(res)
				return res.data;
			});
	}

	createSubdis(requestBody: any) {
		return this._http.post(this.urlPostSubdis, requestBody)
			.then((res: any) => {
				return res.data;
			});
	}

	getImei(nId: number) {
		return this._http.post<IInventoryResponse, any>(this.urlGetInventario, { id: nId }, { showLoader: true })
			.then((res) => {

				return res.data
			})
	}

	getUsuariosVendedores() {
		return this._http.get<IUsuariosVendedoresResponse>(this.urlGetUsuariosVendedores)
			.then(res => {
				return res.data;
			})
	}
}
