import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { IInventorySellerResponse } from 'src/app/models/response/inventorySeller';
import { IConsultaResponse } from 'src/app/models/response/consulta';
import { IInventoryAvailableResponse } from 'src/app/models/response/inventoryAvailable';
import { IProductsResponse } from 'src/app/models/response/products';
import { IProducts } from 'src/app/models/request/products';
import { IStatus } from 'src/app/models/response/status';
import { IReportsActivationsResponse } from 'src/app/models/response/reportsActivations';


@Injectable()
export class ReportsService {

  private urlGetReportQueries = '/buscarRecargasBot'
  private urlGetReportQueriesExcel = '/excelConsultas'

  private serviceBtnBitware = '/obtenerRecargaBot' //GET //CONSULTAS
  private serviceBtnTelcel = '/recargaBot' //POST

  private urlGetInventarioDisponible = '/inventarioDisponible' //BANDEJA STATUS
  private urlgGetExcelInventarioDisponible = '/generarExcelInventarioDisponible'

  private urlGetInventoryBySeller = '/inventarioByUsuario'
  private urlGetExcelInventoryBySeller = '/generarExcel'

  private urlGetProducts = "/catTipoProducto"
  private urlGetStatus = "/estatusSerie";

  private urlGetActivaciones = "/activaciones";
  private urlGetExcelActivaciones = "/activacionesExcel";


  constructor(private _http: CustomHttpService) { }


  getQueries(filter: any): Promise<IConsultaResponse> {
    return this._http.post<IConsultaResponse, any>(`${this.urlGetReportQueries}`, filter)
      .then((res) => {
        return res.data
      })
  }

  async getQueriesExcel(filter: any): Promise<any> {
    return this._http.post<any, any>(`${this.urlGetReportQueriesExcel}`, filter)
      .then((res: any) => {
        return res.data
      })
  }

  getProducts(): Promise<IProductsResponse> {
    return this._http.get<IProductsResponse>(`${this.urlGetProducts}`)
      .then((res: any) => {
        console.log(res)
        return res.data.productos
      })
  }

  getInventorySeller(nIdSeller: string) {
    return this._http.get<IInventorySellerResponse>(`${this.urlGetInventoryBySeller}/${nIdSeller}`)
      .then((res) => {
        return res.data
      })
  }

  async getExcelInventorySeller(nIdSeller: string): Promise<any> {
    return this._http.get<any>(`${this.urlGetExcelInventoryBySeller}/${nIdSeller}`)
      .then((res: any) => {
        return res.data
      })
  }

  getIventory(request): Promise<IInventoryAvailableResponse> {

    return this._http.post<IInventoryAvailableResponse, any>(`${this.urlGetInventarioDisponible}`, request)
      .then((res) => {
        return res.data
      })
  }

  async getExcelInventoryAvailable(request) {
    return this._http.post(`${this.urlgGetExcelInventarioDisponible}`, request, { showLoader: true })
      .then((data: any) => {
        return data.data
      })
  }

  getStatus(request): Promise<IStatus> {
    return this._http.post<IStatus, any>(`${this.urlGetStatus}`, request)
      .then((res) => {
        return res.data;
      });
  }

  getActivaciones(request): Promise<IReportsActivationsResponse> {
    return this._http.post<IReportsActivationsResponse, any>(`${this.urlGetActivaciones}`, request)
      .then((res) => {
        return res.data;
      })
  }

  async getActivacionesExcel(request) {
    return this._http.post(`${this.urlGetExcelActivaciones}`, request)
      .then((res:any) => {
        return res.data;
      })
  }





}
