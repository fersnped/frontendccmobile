import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable()
export class LoginGuard implements CanActivate {

	constructor(
		public _auth: AuthService,
		public _router: Router
	) { }

	canActivate() {
		if (this._auth.isLogged()) {
			if (this._auth.getUser().primer_Login) {
				this._router.navigate(['/login']);
				return false;
			} else
				return true;
		} else {
			this._router.navigate(['/login']);
			return false;
		}

	}
}