import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpHeaders } from '@angular/common/http';
import { BASE_URL, PROD } from 'src/app/config/config';
import { AuthService } from 'src/app/services/auth/auth.service';
import { HttpUtils } from 'src/app/services/http/utils';
import { IApiResponse, IApiRealResponse } from 'src/app/models/response/api';
import { AlertsService } from 'src/app/services/shared/alerts.service';
import { LoaderService } from 'src/app/services/shared/loader.service';
import { SweetAlertOptions } from 'sweetalert2';

/**
 * @property {boolean} auth Default: true
 */
interface IRequestOptions {
	baseUrl?: string;
	headers?: { [s: string]: string };
	query?: { [s: string]: string };
	params?: { [s: string]: string | number };
	auth?: boolean;
	responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
	errorMessage?: boolean | { text?: string, title?: string };
	successMessage?: boolean | { text?: string, title?: string, type?: 'success' | 'info' | 'warning' };
	showLoader?: boolean;
}

interface IInitOptions {
	headers?: HttpHeaders;
	reportProgress?: boolean;
	params?: HttpParams;
	responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
	withCredentials?: boolean;
}
@Injectable({
	providedIn: 'root'
})
export class CustomHttpService {
	baseUrl: string = BASE_URL;
	utils: HttpUtils;
	pendingRequests: number = 0;

	constructor(
		private _http: HttpClient,
		private _auth: AuthService,
		private _alertsService: AlertsService,
		private _loaderService: LoaderService
	) {
		this.utils = new HttpUtils();
	}

	/**
	 * Make get http request
	 * @param url Request url
	 * @param options Request options
	 */
	get<T>(url: string, options?: IRequestOptions): Promise<IApiResponse<T>> {
		return this.handleRequest<T, any>('GET', url, null, options);
	}

	/**
	 * Make post http request
	 * @param url Request url
	 * @param body Request body
	 * @param options Request options
	 */
	post<T, R>(url: string, body: R, options?: IRequestOptions): Promise<IApiResponse<T>> {
		return this.handleRequest<T, R>('POST', url, body, options);
	}
	/**
	 * Make put http request
	 * @param url Request url
	 * @param body Request body
	 * @param options Request options
	 */
	put<T, R>(url: string, body: R, options?: IRequestOptions): Promise<IApiResponse<T>> {
		return this.handleRequest<T, R>('PUT', url, body, options);
	}

	/**
	 * Make patch http request
	 * @param url Request url
	 * @param body Request body
	 * @param options Request options
	 */
	patch<T, R>(url: string, body: R, options?: IRequestOptions): Promise<IApiResponse<T>> {
		return this.handleRequest<T, R>('PATCH', url, body, options);
	}

	/**
	 * Make delete http request
	 * @param url Request url
	 * @param options Request options
	 */
	delete<T>(url: string, options?: any): Promise<IApiResponse<T>> {
		return this.handleRequest<T, any>('DELETE', url, null, options);
	}

	/**
	 * 
	 * @param method Http request method
	 * @param url Request url
	 * @param body Request body
	 * @param options Request options
	 * @property {} [options.auth] Requires auth | default: true
	 */
	private async handleRequest<T, R>(
		method: string,
		url: string,
		body: R | null,
		options: IRequestOptions): Promise<IApiResponse<T>> {
		options = Object.assign(<IRequestOptions>{
			auth: true,
			baseUrl: this.baseUrl,
			responseType: 'json',
			errorMessage: true,
			successMessage: false,
			showLoader: false
		}, options);

		this.turnOnRequest(options.showLoader);

		const fullUrl: string = options.baseUrl
			+ this.utils.transformParams(url, options.params)
			+ this.utils.transformQuery(options.query);

		const init: IInitOptions = {
			headers: new HttpHeaders(this.utils.createHeaders(options.headers || {}, options.auth ? this._auth.getToken() : '')),
			responseType: options.responseType,
			reportProgress: true
		};

		const request = new HttpRequest<R>(method, fullUrl, body, init);
		if (!PROD) console.log('REQUEST', request);

		try {
			const res: any = await this._http.request<IApiRealResponse<T>>(request).toPromise();

			if (!PROD) console.log('RESPONSE [SUCCESS]', res);

			const transformedResponse: IApiResponse<T> = this.utils.tranformApiResponse(<IApiRealResponse<T>>res.body, res.status);

			this.handleSuccessAlert(transformedResponse, options.successMessage);
			this.turnOffRequest();
			return transformedResponse;
		} catch (err) {
			this.handleErrorAlert(err, options.errorMessage);
			this.turnOffRequest();
			return Promise.reject(err);
		}
	}

	private handleErrorAlert(err: any, message: boolean | { text?: string, title?: string }) {
		if (message) {
			let alertOptions: SweetAlertOptions = {
				title: 'Ocurrio un error',
				text: err.responseMessage || err.message || err
			}

			if (typeof message === 'object') {
				alertOptions = Object.assign(alertOptions, message);
			}

			this._alertsService.error(alertOptions);
		}
	}

	private handleSuccessAlert(res: any, message: boolean | { text?: string, title?: string, type?: 'success' | 'info' | 'warning' }) {
		if (message) {
			let alertOptions: SweetAlertOptions = {
				title: 'Proceso exitoso',
				text: res.responseMessage || res.message || res,
				type: 'success'
			}

			if (typeof message === 'object') {
				alertOptions = Object.assign(alertOptions, message);
			}

			this._alertsService.show(alertOptions);
		}
	}

	turnOnRequest(showLoader: boolean): void {
		this.pendingRequests++;
		if (showLoader) this._loaderService.show();
	}

	turnOffRequest(): void {
		this.pendingRequests--;

		if (this.pendingRequests < 1) {
			this._loaderService.hide();
		}
	}

}
