import { IApiRealResponse, IApiResponse } from "src/app/models/response/api";
import { HttpHeaders } from "@angular/common/http";

/**
 * Utils for custom HttpRequest
 */
export class HttpUtils {
	constructor() { }

	createHeaders(headers: { [s: string]: string }, auth?: string): { [s: string]: string } {

		const obj = {}
		if (!headers) {
			obj['Content-Type'] = 'application/json';

		}

		if (auth && auth.trim() !== '') {
			obj['Authorization'] = auth;
		}
		delete headers['Content-Type'];
		
		return {
			...obj,
			...headers
		}
	}

	/**
	 * Transform query object to url query string
	 * @param {Object} obj Query
	 * @returns {String}
	 */
	transformQuery(obj: any) {
		if (!obj) return '';

		return Object.keys(obj).reduce((prev, key, i, arr) => {
			const value = obj[key];
			if (typeof value === 'number' || typeof value === 'string') {
				return prev + (key + '=' + value + (i < arr.length - 1 ? '&' : ''));
			} else {
				return prev + '';
			}
		}, '?');
	}

	/**
	 * Replace url's implicit parameters. 
	 * Example: 
	 * 	transformParams('/users/:id/jobs', { id: '738dad34wwe' });
	 * "/users/:id/jobs" will be replaced for "/users/738dad34wwe/jobs"
	 * @param {String} url Url string to replace parameters
	 * @param {Object} obj Object with attributes to replace in Url
	 */
	transformParams(url: string, obj: { [s: string]: string | number } = {}) {
		const regex = /\/:\s?[\w\s]*\s?/g;
		const replaceColon = /[\/:]/g;

		if (!regex.test(url)) return url;

		return url.match(regex).reduce((prev, item) => {
			const varName = item.replace(replaceColon, '');
			const value = obj[varName] || '';
			const varRegexReplace = new RegExp('\/:\s?(' + varName + ')*\s?');

			return prev.replace(varRegexReplace, value ? '/' + value : '/');
		}, url);
	}

	tranformApiResponse<T>(body: IApiRealResponse<T>, httpCode: number): IApiResponse<T> {
		return {
			data: body.responseResult,
			httpCode,
			message: body.responseMessage,
			success: body.success,
			errors: body.responseErrors
		}
	}

}