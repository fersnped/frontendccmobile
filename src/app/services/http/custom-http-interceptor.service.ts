import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, ObservableInput } from 'rxjs';

import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { PROD } from 'src/app/config/config';
import { HttpUtils } from 'src/app/services/http/utils';
import { IApiRealResponse } from 'src/app/models/response/api';

@Injectable()
export class CustomHttpInterceptorService implements HttpInterceptor {
	pendingRequests: number = 0;
	utils: HttpUtils;

	constructor(
		private _auth: AuthService,
		private _router: Router,
		//private _activatedRoute: ActivatedRoute
	) {
		this.utils = new HttpUtils();
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		this.turnOnRequest();

		const request: HttpRequest<any> = req.clone();

		if (!PROD) console.log('REQUEST', request);

		return next
			.handle(request)
			.pipe(
				map(this.map(this)),
				catchError(this.catch(this))
			);
	}

	map(_thisArg: CustomHttpInterceptorService) {
		return (value: HttpEvent<any>): HttpEvent<any> => {

			if (value instanceof HttpResponse) {
				console.log('value--->>>', value);
			}

			_thisArg.turnOffRequest();

			return value;
		}
	}

	catch(_thisArg: CustomHttpInterceptorService) {
		return (err: any): ObservableInput<never> => {
			if (!PROD) console.log('RESPONSE [ERROR]', err);

			if (err && typeof err.status !== 'undefined') {
				switch (Number(err.status)) {
					case 401:
						this._auth.destroySession();
						this._router.navigate(['/login']);
						break;
				}
			}

			const httpError: HttpErrorResponse = err;
			let sendError: any = null;

			if (httpError.error && typeof httpError.error == 'object' && httpError.error.responseMessage) {
				sendError = this.utils.tranformApiResponse(<IApiRealResponse<any>>httpError.error, httpError.status);
			} else {
				sendError = httpError;
			}

			_thisArg.turnOffRequest();

			return throwError(sendError);
		}
	}

	turnOnRequest(): void {
		this.pendingRequests++;
	}

	turnOffRequest(): void {
		this.pendingRequests--;
	}
}
