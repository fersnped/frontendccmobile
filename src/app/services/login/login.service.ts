import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { ILoginResponse, IChangePasswordResponse, IResetPasswordResponse } from 'src/app/models/response/login';
import { ILoginRequest, IChangePasswordRequest, IResetPasswordRequest } from 'src/app/models/request/login';
import { IApiResponse } from 'src/app/models/response/api';

@Injectable()
export class LoginService {

	constructor(
		private _http: CustomHttpService
	) { }

	login(body: ILoginRequest): Promise<ILoginResponse> {
		return this._http.post<ILoginResponse, ILoginRequest>('/login', body, {
			auth: false,
			showLoader: true
		}).then((res) => {
			return res.data;
		});
	}

	changePassword(body: IChangePasswordRequest): Promise<IApiResponse<IChangePasswordResponse>> {
		return this._http.post<IChangePasswordResponse, IChangePasswordRequest>('/changepassword', body)
	}

	resetPassword(mail: string): Promise<IResetPasswordResponse> {
		return this._http.post<IResetPasswordResponse, IResetPasswordRequest>('/resetpassword', { mail }, {
			auth: false,
			showLoader: true
		}).then((res) => {
			return res.data;
		});
	}


}
