import { Injectable } from "@angular/core";
import { CustomHttpService } from "../http/custom-http.service";
import { IActivation } from "../../models/activation";
import { IActivacionesResponse } from "../../models/response/activations";
import {
  IRecargasDisponibles,
  IRecargasRealizadas
} from "../../models/recargasRealizadas";

@Injectable()
export class RecargasService {
  private urlRecargasDisponibles = "/recargasDisponibles";
  private urlRecargasRealizadas = "/recargasRealizadas";

  constructor(private http: CustomHttpService) {}

  getRecargasDisponibles(
    fechaInicial: string,
    fechaFinal: string,
    numero: string,
    iccid: string,
    nombre_usuario: string
  ): Promise<any> {
    let params = "?";

    params = params.concat(
      fechaInicial !== "" ? "&" + "fecha_inicial=" + fechaInicial  : ""
    );
    params = params.concat(
      fechaFinal !== "" ? "&" + "fecha_final=" + fechaFinal : ""
    );
    params = params.concat(numero !== "" ? "&" + "numero=" + numero : "");
    params = params.concat(iccid !== "" ? "&" + "iccid=" + iccid : "");
    params = params.concat(
      nombre_usuario !== "" ? "&" + "nombre_usuario=" + nombre_usuario : ""
    );

    return this.http
      .get<IRecargasDisponibles[]>(this.urlRecargasDisponibles + params)
      .then(res => {
        return res.data;
      });
  }

  getRecargasRealizadas(
    fechaInicial: string,
    fechaFinal: string,
    numero: string,
    iccid: string,
    nombre_usuario: string
  ): Promise<any> {
    let params = "?";

    params = params.concat(
      fechaInicial !== "" ? "&" + "fecha_inicial=" + fechaInicial : ""
    );
    params = params.concat(
      fechaFinal !== "" ? "&" + "fecha_final=" + fechaFinal : ""
    );
    params = params.concat(numero !== "" ? "&" + "numero=" + numero : "");
    params = params.concat(iccid !== "" ? "&" + "iccid=" + iccid : "");
    params = params.concat(
      nombre_usuario !== "" ? "&" + "nombre_usuario=" + nombre_usuario : ""
    );

    return this.http
      .get<IRecargasRealizadas[]>(this.urlRecargasRealizadas + params)
      .then(res => {
        return res.data;
      });
  }
}
