import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { IApiResponse } from 'src/app/models/response/api';
import { IRollsResponse } from 'src/app/models/response/roll';
import { IRollRequest } from 'src/app/models/request/roll';
import { Roll } from 'src/app/models/roll';

@Injectable()
export class RollsService {
	private url: string = '/rolls';

	constructor(
		private _http: CustomHttpService
	) { }

	get(): Promise<IRollsResponse> {
		return this._http.get<IRollsResponse>(this.url)
			.then((res: any) => {
				return res.data.rolls;
			});
	}

	getById(id: number): Promise<any> {
		return this._http.get<any>(`${this.url}/:id`, {
			params: {
				id
			}
		}).then((res) => {
			return res.data;
		});
	}

	create(roll: any): Promise<IApiResponse<any>> {
		return this._http.post<any, IRollRequest>(this.url, roll)
			.then((res) => {
				return res;
			});
	}

	update(id: number, roll: any): Promise<IApiResponse<any>> {
		return this._http.put<any, IRollRequest>(`${this.url}/:id`, roll, {
			params: {
				id
			}
		});
	}

	toggleActive(id: number): Promise<IApiResponse<any>> {
		return this._http.get<any>(`${this.url}/:id/lock`, {
			params: {
				id
			}
		});
	}

	delete(id: number): Promise<IApiResponse<any>> {
		return this._http.delete<any>(`${this.url}/:id`, {
			params: {
				id
			}
		});
	}

}
