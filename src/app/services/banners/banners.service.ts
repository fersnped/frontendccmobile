import { Injectable } from '@angular/core';
import { CustomHttpService } from '../http/custom-http.service';
import { IBannersRequest, IBannerRequest } from 'src/app/models/response/banners';
import { IApiResponse } from 'src/app/models/response/api';

@Injectable({
  providedIn: 'root'
})
export class BannersService {

  constructor(
    private _http: CustomHttpService
  ) { }

  getBanners(): Promise<IBannersRequest> {
    return this._http.get<IBannersRequest>("/getBanners")
      .then((res: any) => {
        console.log(res.data.banners)
        return res.data.banners
        
      })
  }

  getBanner(nIdBanner: number): Promise<IBannerRequest> {
    return this._http.get<IBannerRequest>("/getBannersById/" + nIdBanner)
      .then((res: any) => {
        return res.data.banner;
      })
  }

  postBanner(formData: FormData): Promise<any> {
    return this._http.post<any, FormData>("/postBanner", formData, { headers: { 'Content-Type': 'multipart/form-data' } })
      .then((res: any) => {
        return res.data
      })
  }

  deleteBanner(nIdBanner: number): Promise<IApiResponse<any>> {
    return this._http.delete("/eliminarBanner/" + nIdBanner)
      .then((res: any) => {
        return res.data.message
      })
  }

  updateBanner(idBanner: number, formData: FormData): Promise<any> {
    return this._http.put<any, FormData>("/putBanner/" + idBanner, formData, { headers: { 'Content-Type': 'multipart/form-data' } })
      .then((res: any) => {
        return res.data
      })
  }



}
