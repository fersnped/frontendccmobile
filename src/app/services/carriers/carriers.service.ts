import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { ICarriersResponse } from 'src/app/models/response/carrier';

@Injectable()
export class CarriersService {
	urlService = '/carrier';

	constructor(
		private _http: CustomHttpService
	) { }

	get(): Promise<any[]> {
		// return this._http.get<ICarriersResponse>(this.urlService)
		return this._http.get<any>(this.urlService)
			.then((res: any) => {
				return res.data.carrier[0];
			});
	}

}
