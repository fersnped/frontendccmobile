import { Injectable } from '@angular/core';
import { CustomHttpService } from '../http/custom-http.service';
import { IActivation } from '../../models/activation';
import { IActivacionesResponse } from '../../models/response/activations';
import { IActivationDetail } from '../../models/activationDetail';
import { ISubdistribuidor } from '../../models/subdistribuidor';
import { ISubdistribuidoresResponse } from '../../models/response/subdistribuidores';
import { IImei } from '../../models/imei';
import { IIccid } from '../../models/iccid';
import { ICiudad } from '../../models/ciudad';
import { ICiudadesResponse } from '../../models/response/ciudades';
import { ICatalogo } from '../../models/catalogo';
import { BehaviorSubject } from 'rxjs';
import { ActivationConfirm, IActivationConfirm } from '../../models/activationConfirm';
import { IActivacionesDetailResponse } from '../../models/response/activationDetail';

@Injectable()
export class ActivationService {

  private urlAllActivations = '/getActivacion';
  private urlActivation = '/getActivacionById';
  private urlDistribuidores = '/getSubdistribuidor';
  private urlValidateImei = '/validarIMEI';
  private urlValidateIccid = '/validarICCID';
  private urlCiudades = '/getCiudades';
  private urlCatalogos = '/getCatalogos';
  private urlCrearActivacion = '/postActivacion';
  private urlRecargar = '/recargar'

  private confirmacionSource = new BehaviorSubject(new ActivationConfirm());
  currentConfirmacion = this.confirmacionSource.asObservable();

  constructor(private http: CustomHttpService) {
  }

  getAll(id: number): Promise<IActivation[]> {
    return this.http.get<IActivacionesResponse>(`${this.urlAllActivations}/:id`, {
      params: {
        id
      }
    }).then((res) => {
      return res.data.activaciones;
    });
  }

  getActivationByFolio(folio: string): Promise<IActivationDetail> {
    return this.http.get<IActivacionesDetailResponse>(`${this.urlActivation}/:folio`, {
      params: {
        folio
      }
    }).then((res) => {
      return res.data.activacion;
    });
  }

  getDistribuidores(): Promise<ISubdistribuidor[]> {
    return this.http.get<ISubdistribuidoresResponse>(this.urlDistribuidores)
      .then((res) => {
        return res.data.subdistribuidor;
      });
  }

  async validateImei(imei: string): Promise<IImei> {
    return this.http.get<IImei>(`${this.urlValidateImei}/:imei`, {
      params: {
        imei
      }
    }).then((res) => {
      console.log('Response: ', res);
      return res.data;
    });
  }


  validateIccid(iccid: string): Promise<IIccid> {
    return this.http.get<IIccid>(`${this.urlValidateIccid}/:iccid`, {
      params: {
        iccid
      }
    }).then((res) => {
      return res.data;
    });
  }

  getCiudades(): Promise<ICiudad[]> {
    return this.http.get<ICiudadesResponse>(this.urlCiudades)
      .then((res) => {
        return res.data.ciudades;
      });
  }

  getCatalogos(): Promise<ICatalogo> {
    return this.http.get<ICatalogo>(this.urlCatalogos)
      .then((res) => {
        return res.data;
      });
  }

  crearActivacion(body: any): Promise<IActivationDetail> {
    return this.http.post<IActivacionesDetailResponse, IActivationDetail>(this.urlCrearActivacion, body)
      .then(res => {
        return res.data.activacion;
      });
  }

  changeConfirmacion(confirmacion: IActivationConfirm) {
    if (confirmacion instanceof ActivationConfirm) {
      this.confirmacionSource.next(confirmacion);
    }
  }

  recargarLinea(id) {
    return this.http.post(this.urlRecargar + '/' + id, {}, { showLoader: true })
      .then((res) => {
        return res.message
      }).catch(res => {
        throw (res.message)
      })
  }
}
