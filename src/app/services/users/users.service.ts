import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';
import { IApiResponse } from 'src/app/models/response/api';
import { IUser } from 'src/app/models/user';
import { IUsersResponse } from 'src/app/models/response/user';
import { IUserRequest, UserRequest } from 'src/app/models/request/users';
import { IPerfil } from 'src/app/models/perfil';

@Injectable()
export class UsersService {
	private url: string = '/users';
	private urlProfiles: string = '/perfiles';

	constructor(
		private _http: CustomHttpService
	) { }

	/**
	 * Get list of users
	 */
	get(): Promise<IUser[]> {
		return this._http.get<IUsersResponse>(this.url)
			.then((res) => {
				return res.data.users;
			});
	}

	/**
	 * Get user by id
	 * @param id IUser id to consult
	 */
	getById(id: number): Promise<IUser> {
		return this._http.get<IUser>(`${this.url}/:id`, {
			params: {
				id
			}
		}).then((res) => {
			return res.data;
		});
	}

	/**
	 * Create user
	 * @param user IUser object
	 */
	create(user: any): Promise<IApiResponse<any>> {

		return this._http.post<any, any>(this.url, user,{showLoader: true});
	}

	/**
	 * Update user
	 * @param id IUser id to update
	 * @param user IUser object
	 */
	update(id: number, user: any): Promise<IApiResponse<any>> {
		// console.log(JSON.parse(user))
		return this._http.put<any, any>(`${this.url}/:id`, user, {
			params: {
				id
			},
			showLoader: true
		});
	}

	/**
	 * Toggle active state for user
	 * @param id IUser id to change active state
	 */
	toggleActive(id: number): Promise<IApiResponse<null>> {
		return this._http.get<null>(`${this.url}/:id/lock`, {
			params: {
				id
			}
		});
	}

	/**
	 * Delete user
	 * @param id IUser id to delete
	 */
	delete(id: number): Promise<IApiResponse<null>> {
		return this._http.delete<null>(`${this.url}/:id`, {
			params: {
				id
			}
		});
	}
	getProfiles() {
		return this._http.get<IPerfil[]>(this.urlProfiles)
		.then((res) => {
			return res.data;
		});
		
	}
}
