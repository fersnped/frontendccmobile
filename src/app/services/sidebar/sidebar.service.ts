import { Injectable, Inject } from "@angular/core";
//import { DOCUMENT } from "@angular/common";

@Injectable()
export class SidebarService {

    nMenuOpen: any = {
        nModulo: 0,
        isClicked: false
    }
    constructor() {

    }

    setMenuOption(nModulo: number) {
        localStorage.setItem("nMenuOpen", JSON.stringify({ nModulo: nModulo, isClicked: false }));
    }

    setIsClicked(bFlag: boolean) {
        if (localStorage.getItem("nMenuOpen")) {
            this.nMenuOpen = JSON.parse(localStorage.getItem("nMenuOpen"));
            this.nMenuOpen.isClicked = bFlag
            localStorage.setItem("nMenuOpen", JSON.stringify(this.nMenuOpen));
        }
        else {
            return this.nMenuOpen
        }
    }

    getMenuOption(): any {
        if (localStorage.getItem("nMenuOpen")) {
            return this.nMenuOpen = JSON.parse(localStorage.getItem("nMenuOpen"));
        }
        else {
            return this.nMenuOpen
        }
    }

}