import { Injectable } from '@angular/core';
import swal, { SweetAlertOptions, SweetAlertType, SweetAlertResult } from 'sweetalert2';

@Injectable()
export class AlertsService {
	configAlert: { [s: string]: string | boolean } | SweetAlertOptions = {
		confirmButtonClass: 'waves-effect waves-light btn blue accent-1 btn-alert',
		cancelButtonClass: 'waves-effect waves-light btn blue accent-1 btn-alert',
		confirmButtonText: 'Aceptar',
		allowOutsideClick: false,
		allowEscapeKey: false,
		cancelButtonText: 'Cancelar',
	};

	constructor() { }

	success(alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		return this.handleAlert('success', alert);
	}

	warning(alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		return this.handleAlert('warning', alert);
	}

	question(alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		return this.handleAlert('question', alert);
	}

	info(alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		return this.handleAlert('info', alert);
	}

	error(alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		return this.handleAlert('error', alert);
	}

	show(alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		return this.handleAlert(alert.type || 'success', alert);
	}

	private handleAlert(type: SweetAlertType, alert: SweetAlertOptions = {}): Promise<SweetAlertResult> {
		alert.type = type;

		return swal.fire(Object.assign({}, this.configAlert, alert));
	}

}
