import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Label } from "src/app/models/label";
import { IApiResponse } from "src/app/models/response/api";
import { AuthService } from "../auth/auth.service";
import { CustomHttpService } from "../http/custom-http.service";
import { HttpUtils } from "../http/utils";

@Injectable({
  providedIn: "root"
})
export class QrService {
  utils: HttpUtils;

  constructor(
    private _http: CustomHttpService,
    private http: HttpClient,
    private _auth: AuthService
  ) {
    this.utils = new HttpUtils();
  }

  /**
   *
   * @param none
   */
  getFiles(): Promise<any> {
    return this._http.get<any>("/getQR").then((res: any) => {
      console.log(res);
      return res.data;
    });
  }
  /**
   *
   * @param none
   */
  getLabels(): Promise<any> {
    return this._http.get<any>("/tags").then((res: any) => {
      console.log(res);
      return res.data;
    });
  }
  /**
   *
   * @param none
   */
  postLabels(label: Label): Promise<any> {
    return this._http.post<any, Label>("/tags", label).then((res: any) => {
      console.log(res);
      return res.data;
    });
  }
  /**
   *
   * @param none
   */
  patchLabels(idLabel: number, label: Label): Promise<any> {
    return this._http
      .put<any, Label>(`/tags/${idLabel}`, label)
      .then((res: any) => {
        console.log(res);
        return res.data;
      });
  }
  /**
   *
   * @param none
   */
  deleteLabels(idLabel: number): Promise<any> {
    return this._http.delete<any>(`/tags/${idLabel}`).then((res: any) => {
      console.log(res);
      return res.data;
    });
  }

  /**
   * Post file
   * @param formData File to Send
   */
  async postFile(formData: FormData): Promise<any> {
    return this._http
      .post<any, FormData>("/qr", formData, {
        headers: { "Content-Type": "multipart/form-data" }
      })
      .then((res: any) => {
        return res.data;
      })
      .catch((err: any) => {
        throw new Error(err.message);
      });
  }

  /**
   * Confirmation File Charge
   */
  confirmFile(bOption: boolean): Promise<IApiResponse<any>> {
    return this._http
      .post<any, any>("/confirmar", { opcion: bOption })
      .then((res: any) => {
        console.log(res);
        return res.data;
      });
  }

  qrConfirmFile(iccids, idArchivo): Promise<IApiResponse<any>> {
    return this._http
      .post<any, any>("/qrConfirmar", {
        recargas: iccids,
        idArchivo: idArchivo
      })
      .then((res: any) => {
        console.log(res);
        return res.data;
      });
  }

  /**
   *
   * @param nIdFile Id file to get lines
   */
  getLineas(nIdFile: number): Promise<any> {
    return this._http.get<any>("/getDetailsQR/" + nIdFile).then((res: any) => {
      console.log(res);
      return res.data;
    });
  }

  /**
   *
   * @param idPdf Id file to get lines
   */
  downloadPdf(idPdf: number, idLabel: number): Promise<any> {
    return this._http.get<any>(`/pdf/${idPdf}/${idLabel}`).then((res: any) => {
      return res.data;
    });
  }
}
