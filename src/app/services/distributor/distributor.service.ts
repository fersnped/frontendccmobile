import { Injectable } from '@angular/core';
import { CustomHttpService } from 'src/app/services/http/custom-http.service';

@Injectable()
export class DistributorService {
	private url: string = '/getSubdistribuidor';

	constructor(
		private _http: CustomHttpService
	) { }

	/**
	 * Get list of
	 */
	get(): Promise<any[]> {
		return this._http.get<any>(this.url)
			.then((res) => {
				return res.data.subdistribuidor;
			});
  }

  create(objDistribuidor): Promise<any> {
		return this._http.post<any, any>(this.url, { objDistribuidor });
	}
}
