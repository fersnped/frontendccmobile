import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from 'src/app/services/shared/loader.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'ag-main-loader',
	templateUrl: './loader.component.html',
	styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {
	private subscription: Subscription;
	show: boolean = false;

	constructor(
		private _loader: LoaderService
	) { }

	ngOnInit() {
		this.subscription = this._loader.loaderState
			.subscribe((value) => {
				this.show = value;
			});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}
